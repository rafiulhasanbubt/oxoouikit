//
//  APICalls.swift
//  OxooHomePage
//
//  Created by SpaGreen on 11/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import Foundation
import UIKit

//MARK : - HTTP request handle
extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
    }
}

//MARK: Get data with header parameter with codable
func getDataWithHeaderCodable(url : String, completion: @escaping (_ response: Data) -> Void) {
    let Username = Webservice.userName
    let password = Webservice.password
    let loginData = String(format: "%@:%@", Username, password).data(using: String.Encoding.utf8)!
    let base64LoginData = loginData.base64EncodedString()
    
    let jsonURLString = url
    
    guard let url = URL(string: jsonURLString) else { return }
    
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.addValue("this-is-my-key", forHTTPHeaderField: "API-KEY")
    request.addValue("Basic Auth", forHTTPHeaderField: "Authorization")
    request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let data = data else { return }
        completion(data)
    }.resume()
}

func getDataWithHeaderAndArray(url : String, completion: @escaping (_ response: NSArray) -> Void) {
    let Username = Webservice.userName
    let password = Webservice.password
    let loginData = String(format: "%@:%@", Username, password).data(using: String.Encoding.utf8)!
    let base64LoginData = loginData.base64EncodedString()
    
    let jsonURLString = url
    
    guard let url = URL(string: jsonURLString) else { return }
    
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.addValue("this-is-my-key", forHTTPHeaderField: "API-KEY")
    request.addValue("Basic Auth", forHTTPHeaderField: "Authorization")
    request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let data = data else { return }
        do {
            let response = try JSONSerialization.jsonObject(with: data) as? NSArray
            completion(response!)
        } catch {
            print(error.localizedDescription)
            print("Error in Decoding : \(error)")
            DispatchQueue.main.async {
                
            }
        }
    }.resume()
}

func getDataWithHeaderArrayCodable(url : String, completion: @escaping (_ response: Data) -> Void) {
    let Username = Webservice.userName
    let password = Webservice.password
    let loginData = String(format: "%@:%@", Username, password).data(using: String.Encoding.utf8)!
    let base64LoginData = loginData.base64EncodedString()
    
    let jsonURLString = url
    
    guard let url = URL(string: jsonURLString) else { return }
    
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.addValue("this-is-my-key", forHTTPHeaderField: "API-KEY")
    request.addValue("Basic Auth", forHTTPHeaderField: "Authorization")
    request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let data = data else { return }
        completion(data)
    }.resume()
}

//MARK: Get data with header and Query Parameter with codable
func getDataWithHeaderAndParameter(url : String, id: String, completion: @escaping (_ response: Data) -> Void) {
    let Username = Webservice.userName
    let password = Webservice.password
    let loginData = String(format: "%@:%@", Username, password).data(using: String.Encoding.utf8)!
    let base64LoginData = loginData.base64EncodedString()
    
    //MARK: query parameter added from appending extension with URL
    let urls = URL(string: url)!
    let finalURL = urls.appending("id", value: id)
    
    var request = URLRequest(url: finalURL)
    request.httpMethod = "GET"
    request.addValue("this-is-my-key", forHTTPHeaderField: "API-KEY")
    request.addValue("Basic Auth", forHTTPHeaderField: "Authorization")
    request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
    
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let data = data else { return }
        completion(data)
    }.resume()
}

//MARK: Get data with header and Body parameter with codable
func getDataWithHeaderAndBody(url : String, newBody : [String : String] , completion: @escaping (_ response: Data) -> Void) {
    let Username = Webservice.userName
    let password = Webservice.password
    let loginData = String(format: "%@:%@", Username, password).data(using: String.Encoding.utf8)!
    let base64LoginData = loginData.base64EncodedString()
    
    let jsonURLString = url
    
    guard let url = URL(string: jsonURLString) else { return }
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("this-is-my-key", forHTTPHeaderField: "API-KEY")
    request.addValue("Basic Auth", forHTTPHeaderField: "Authorization")
    request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
    request.httpBody = newBody.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let data = data else { return }
        completion(data)
    }.resume()
}


extension URL {
    func appending(_ queryItem: String, value: String?) -> URL {
        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }
        // Create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        // Create query item
        let queryItem = URLQueryItem(name: queryItem, value: value)
        // Append the new query item in the existing query items array
        queryItems.append(queryItem)
        // Append updated query items array in the url component object
        urlComponents.queryItems = queryItems
        // Returns the url from new url components
        return urlComponents.url!
    }
}
