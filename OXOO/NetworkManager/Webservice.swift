//
//  Webservice.swift
//  OxooHomePage
//
//  Created by SpaGreen on 4/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import Foundation
import UIKit

struct Webservice {
    static let userName = "admin"
    static let password = "1234"
    static let baseurl = "http://demo.redtvlive.com/v320/api/v100/"
    
    static let homeContent = "\(baseurl)home_content"
    static let movies = "\(baseurl)movies"
    static let allTVChannel = "\(baseurl)all_tv_channel_by_category"
    static let TVSeries = "\(baseurl)tvseries"
    static let allGenre = "\(baseurl)all_genre"
    static let movieByGenre = "\(baseurl)movie_by_genre_id"
    static let allCountry = "\(baseurl)all_country"
    static let movieByCountry = "\(baseurl)movie_by_country_id"
    static let details = "\(baseurl)single_details"
}
