//
//  CountryViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 31/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class CountryViewController: BaseViewController {
    
    @IBOutlet var countryCollectionView: UICollectionView!
    
    var countryArray = [CountryDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton()
        // Do any additional setup after loading the view.
        countryCollectionView.dataSource = self
        countryCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 100, height: 80)
        flowLayout.minimumLineSpacing = 8
        flowLayout.minimumInteritemSpacing = 8.0
        self.countryCollectionView.collectionViewLayout = flowLayout
        
        let countryNib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        countryCollectionView.register(countryNib, forCellWithReuseIdentifier: "categoryCell")
        
        getCountryData()
    }
    
    // API func created
    func getCountryData(){
        self.showSpinner(onView: self.view)
        let base = Webservice.allCountry
        
        getDataWithHeaderCodable(url: base) { response in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode([CountryDataModel].self, from: data)
                for countryModel in result {
                    self.countryArray.append(countryModel)
                }
                DispatchQueue.main.async {
                    self.countryCollectionView.reloadData()
                    self.removeSpinner()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        
    }
    
}


extension CountryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return countryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
        
        cell.categoryNameLabel.text = countryArray[indexPath.item].name
        
        guard let ImageURL = URL(string: countryArray[indexPath.item].imageURL) else { return cell }
        let data = try? Data(contentsOf: ImageURL)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            cell.categoryImageView.image = image
        }
        cell.gradientColor()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        let height = 80
        
        return CGSize(width: size, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let countryID = countryArray[indexPath.item].countryID
        let categoryView: CountryCategoryViewController = self.storyboard?.instantiateViewController(identifier: "CountryCategoryViewController") as!
        CountryCategoryViewController
        categoryView.countryId = countryID
        self.navigationController?.pushViewController(categoryView, animated: true)
    }
    
}
