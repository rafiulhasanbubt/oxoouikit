//
//  GenreViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 31/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class GenreViewController: BaseViewController {
    
    @IBOutlet weak var genericCollectionView: UICollectionView!
    
    var genreArray = [GenreDataModel]()
    
    let colors = [UIColor.light_green_400, UIColor.indigo_400, UIColor.red_400, UIColor.blue_400, UIColor.orange_400, UIColor.blue_grey_400]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton()
        // Do any additional setup after loading the view.
        genericCollectionView.dataSource = self
        genericCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 100, height: 80)
        flowLayout.minimumInteritemSpacing = 8.0
        self.genericCollectionView.collectionViewLayout = flowLayout
        
        let genreNib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        genericCollectionView.register(genreNib, forCellWithReuseIdentifier: "categoryCell")
        // API func called
        getGenreData()
    }
    
    // API func created
    func getGenreData(){
        self.showSpinner(onView: self.view)
        let base = Webservice.allGenre
        
        getDataWithHeaderCodable(url: base) { response in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode([GenreDataModel].self, from: data)
                for genreModel in result {
                    self.genreArray.append(genreModel)
                }
                DispatchQueue.main.async {
                    self.genericCollectionView.reloadData()
                    self.removeSpinner()
                }
            } catch {
                print(error.localizedDescription)
            }
            
        }
        
    }
    
}


extension GenreViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genreArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
        
        guard let ImageURL = URL(string: genreArray[indexPath.item].imageURL) else { return cell }
        let data = try? Data(contentsOf: ImageURL)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            cell.categoryImageView.image = image
        }
        cell.categoryNameLabel.text = genreArray[indexPath.item].name
        cell.gradientColor()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfItemInRow = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfItemInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfItemInRow))
        let height = 80
        
        return CGSize(width: size, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let genreid = genreArray[indexPath.item].genreID
        let categoryView: GenreCategoryViewController = self.storyboard?.instantiateViewController(identifier: "CategoryViewController") as! GenreCategoryViewController
        categoryView.genreId = genreid
        self.navigationController?.pushViewController(categoryView, animated: true)
    }
    
}
