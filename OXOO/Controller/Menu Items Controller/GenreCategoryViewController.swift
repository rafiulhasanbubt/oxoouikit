//
//  GenreCategoryViewController.swift
//  OXOO
//
//  Created by SpaGreen on 3/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class GenreCategoryViewController: UIViewController {
    
    @IBOutlet weak var genreMovieCollectionView: UICollectionView!
    var genreId: String = ""
    var genreMovieArray = [MovieDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.genreMovieCollectionView.dataSource = self
        self.genreMovieCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumLineSpacing = 8.0
        flowLayout.minimumInteritemSpacing = 8.0
        self.genreMovieCollectionView.collectionViewLayout = flowLayout
        
        let movieNib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        genreMovieCollectionView.register(movieNib, forCellWithReuseIdentifier: "movieCell")        
        // Do any additional setup after loading the view.
        //let parameters: [String: String] = ["id": genreId]
        getGenreMovieData(genreID: genreId)
    }
    
    // API func created
    func getGenreMovieData(genreID: String ){
        self.showSpinner(onView: self.view)
        let base = Webservice.movieByGenre
        
        getDataWithHeaderAndParameter(url: base, id: genreId) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode([MovieDataModel].self, from: data)
                for movieModel in result {
                    self.genreMovieArray.append(movieModel)
                }
                DispatchQueue.main.async {
                    self.genreMovieCollectionView.reloadData()
                    self.removeSpinner()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}


extension GenreCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genreMovieArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        let movieData = genreMovieArray[indexPath.item]
        cell.updateMovieCell(movieData: movieData)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfItemInRow = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfItemInRow - 1))
        
        let width = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfItemInRow))
        let height = 250
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movieID = genreMovieArray[indexPath.item].videosID
        let categoryView: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        categoryView.videoID = movieID
        self.navigationController?.pushViewController(categoryView, animated: true)
    }
}
