//
//  CountryCategoryViewController.swift
//  OXOO
//
//  Created by SpaGreen on 29/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class CountryCategoryViewController: UIViewController {
    
    @IBOutlet weak var countryMovieCollectionView: UICollectionView!
    var countryId: String = ""
    var countryMovieArray = [MovieDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.countryMovieCollectionView.dataSource = self
        self.countryMovieCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumLineSpacing = 8.0
        flowLayout.minimumInteritemSpacing = 8.0
        self.countryMovieCollectionView.collectionViewLayout = flowLayout
        
        let movieNib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        countryMovieCollectionView.register(movieNib, forCellWithReuseIdentifier: "movieCell")
        
        getCountryMovieData(countryID: countryId)
    }
    
    // API func created
    func getCountryMovieData(countryID: String ){
        self.showSpinner(onView: self.view)
        let base = Webservice.movieByCountry // + "&&id=\(countryID)"
        
        getDataWithHeaderAndParameter(url: base, id: countryID) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode([MovieDataModel].self, from: data)
                for movieModel in result {
                    self.countryMovieArray.append(movieModel)
                }
                DispatchQueue.main.async {
                    self.countryMovieCollectionView.reloadData()
                    self.removeSpinner()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

extension CountryCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return countryMovieArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        let movieData = countryMovieArray[indexPath.item]
        cell.updateMovieCell(movieData: movieData)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfItemInRow = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfItemInRow - 1))
        
        let width = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfItemInRow))
        let height = 250
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let countryID = countryMovieArray[indexPath.item].videosID
        let categoryView: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        categoryView.videoID = countryID
        self.navigationController?.pushViewController(categoryView, animated: true)
    }
}
