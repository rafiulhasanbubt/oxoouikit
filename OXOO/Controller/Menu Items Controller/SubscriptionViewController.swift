//
//  SubscriptionViewController.swift
//  OXOO
//
//  Created by SpaGreen on 8/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class SubscriptionViewController: UIViewController {

    @IBOutlet weak var upgradeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        upgradeView.layer.cornerRadius = 10
        upgradeView.layer.borderColor = UIColor.lightGray.cgColor
    }

}
