//
//  ProfileViewController.swift
//  OXOO
//
//  Created by SpaGreen on 8/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullnameLabel: UITextField!
    @IBOutlet weak var profileEmailTF: UITextField!
    @IBOutlet weak var profilePasswordTF: UITextField!
    
    @IBOutlet weak var profileButtonView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func profileSaveButtonPressed(_ sender: UIButton) {
        
    }
}
