//
//  LiveTVViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 24/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class LiveTVViewController: BaseViewController {
    
    @IBOutlet weak var channelCategoryTableView: UITableView!
    
    var channelCateArray = [TVChannelCategoryDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        channelCategoryTableView.dataSource = self
        channelCategoryTableView.delegate = self
        
        // Do any additional setup after loading the view.
        //slidemenu added
        addSlideMenuButton()
        
        let channelNib = UINib(nibName: "ChannelCategoryTableViewCell", bundle: nil)
        channelCategoryTableView.register(channelNib, forCellReuseIdentifier: "categoryCell")
        
        channelCategoryTableView.tableFooterView = UIView()
        channelCategoryTableView.rowHeight = UITableView.automaticDimension
        channelCategoryTableView.estimatedRowHeight = 200
        channelCategoryTableView.separatorStyle = .none
        // API func called
        getChannelCategoryData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barStyle = .black
    }
    
    // API func created
    func getChannelCategoryData(){
        
        self.showSpinner(onView: self.view)
        let base = Webservice.allTVChannel
        
        getDataWithHeaderCodable(url: base ) { response in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode([TVChannelCategoryDataModel].self, from: data)

                for categoryModel in result {
                    self.channelCateArray.append(categoryModel)
                }
                DispatchQueue.main.async {
                    self.channelCategoryTableView.reloadData()
                    self.removeSpinner()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

extension LiveTVViewController: UITableViewDataSource, UITableViewDelegate, CustomTVCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channelCateArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! ChannelCategoryTableViewCell
        cell.cellDelegate = self
        cell.channelCategoryLabel.text = channelCateArray[indexPath.row].title
        cell.channelArray = channelCateArray[indexPath.row].channels
        return cell
    }

    func collectionView(collectioncell: ChannelCollectionViewCell?, didTappedInTableview TableCell: ChannelCategoryTableViewCell, detailID: String) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailController = storyBoard.instantiateViewController(withIdentifier:"TVDetailViewController") as! TVDetailViewController
        
        detailController.tvID = detailID
        detailController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
}
