//
//  MoviesViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 24/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class MoviesViewController: BaseViewController {
    
    @IBOutlet weak var allMovieCollectionView: UICollectionView!
    
    var allMovieArray = [MovieDataModel]()
    var loadingView: LoadingReusableView?
    var isLoading = false
    var limit = 0
    var offset = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //slidemenu added
        addSlideMenuButton()
        
        // Do any additional setup after loading the view.
        self.allMovieCollectionView.dataSource = self
        self.allMovieCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumLineSpacing = 8.0
        flowLayout.minimumInteritemSpacing = 8.0
        self.allMovieCollectionView.collectionViewLayout = flowLayout
        
        let movieNib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        allMovieCollectionView.register(movieNib, forCellWithReuseIdentifier: "movieCell")
        
        //Register Loading Reuseable View
        let loadingNib = UINib(nibName: "LoadingReusableView", bundle: nil)
        allMovieCollectionView.register(loadingNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "loadingCell")
        
        // API func called
        getAllMovieData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barStyle = .black
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                limit += 10
                offset += 10
                print("limit : \(limit), offset : \(offset)")
                getAllMovieData()
            }
        }
    
    // API func created
    func getAllMovieData(){
        let base = Webservice.movies
        getDataWithHeaderCodable(url: base) { response in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode([MovieDataModel].self, from: data)
                for movieModel in result {
                    self.allMovieArray.append(movieModel)
                }
                DispatchQueue.main.async {
                    self.allMovieCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}


extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allMovieArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        cell.movieTitleLabel.text = allMovieArray[indexPath.item].title
        cell.releaseLabel.text = allMovieArray[indexPath.item].release
        cell.categoryLabel.text = allMovieArray[indexPath.item].videoQuality
        cell.movieImageView.image = UIImage(named: "movie roll")
        
        guard let ImageURL = URL(string: allMovieArray[indexPath.item].posterURL) else { return cell }
        let data = try? Data(contentsOf: ImageURL)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            cell.movieImageView.image = image
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfItemInRow = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfItemInRow - 1))
        
        let width = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfItemInRow))
        let height = 250
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movieID = allMovieArray[indexPath.item].videosID
        //print("movie id: ",movieID)
        let categoryView: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        categoryView.videoID = movieID
        //hide bottom tar bar
        categoryView.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(categoryView, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.isLoading {
            return CGSize.zero
        } else {
            return CGSize(width: collectionView.bounds.size.width, height: 50)
        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "loadingCell", for: indexPath) as! LoadingReusableView
            loadingView = aFooterView
            loadingView?.backgroundColor = UIColor.clear
            return aFooterView
        }
        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.startAnimating()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.stopAnimating()
        }
    }
}
