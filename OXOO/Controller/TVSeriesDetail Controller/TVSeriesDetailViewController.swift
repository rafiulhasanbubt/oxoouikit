//
//  TVSeriesDetailViewController.swift
//  OXOO
//
//  Created by SpaGreen on 30/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class TVSeriesDetailViewController: UIViewController {
    
    @IBOutlet weak var seriesHeaderGenreCollectionView: UICollectionView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!
    
    @IBOutlet weak var seasonCollectionView: UICollectionView!    
    @IBOutlet weak var episodeCollectionView: UICollectionView!
    @IBOutlet weak var seriesDirectorCollectionView: UICollectionView!
    @IBOutlet weak var seriesGenreCollectionView: UICollectionView!
    @IBOutlet weak var seriesCastCollectionView: UICollectionView!
    @IBOutlet weak var seriesRelatedCollectionView: UICollectionView!
    
    var videoID: String = ""
    var type: String = "tvseries"
    
    var tvseriesDataArray = [TVSeriesDataModel]()
    var seasonArray = [Season]()
    var episodeArray = [Episode]()
    var seriesDirectorArray = [Cast]()
    var seriesCastArray = [Cast]()
    var seriesGenreArray = [Genre]()
    var seriesRelatedArray = [RelatedTvsery]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        seriesHeaderGenreCollectionView.dataSource = self
        seriesHeaderGenreCollectionView.delegate = self
        seasonCollectionView.dataSource = self
        seasonCollectionView.delegate = self
        episodeCollectionView.dataSource = self
        episodeCollectionView.delegate = self
        seriesDirectorCollectionView.dataSource = self
        seriesDirectorCollectionView.delegate = self
        seriesGenreCollectionView.dataSource = self
        seriesGenreCollectionView.delegate = self
        seriesCastCollectionView.dataSource = self
        seriesCastCollectionView.delegate = self
        seriesRelatedCollectionView.dataSource = self
        seriesRelatedCollectionView.delegate = self
        //datasource and delegate called
        
        let directorLayout = UICollectionViewFlowLayout()
        directorLayout.scrollDirection = .horizontal
        directorLayout.itemSize = CGSize(width: 100, height: 140)
        directorLayout.minimumInteritemSpacing = 8.0
        self.seriesDirectorCollectionView.collectionViewLayout = directorLayout
        
        let seasonLayout = UICollectionViewFlowLayout()
        seasonLayout.scrollDirection = .vertical
        seasonLayout.itemSize = CGSize(width: 200, height: 44)
        seasonLayout.minimumInteritemSpacing = 8.0
        self.seasonCollectionView.collectionViewLayout = seasonLayout
        
        let episodeLayout = UICollectionViewFlowLayout()
        episodeLayout.scrollDirection = .horizontal
        episodeLayout.itemSize = CGSize(width: 130, height: 250)
        episodeLayout.minimumInteritemSpacing = 8.0
        self.episodeCollectionView.collectionViewLayout = episodeLayout
        
        
        let genreLayout = UICollectionViewFlowLayout()
        genreLayout.scrollDirection = .horizontal
        genreLayout.itemSize = CGSize(width: 100, height: 44)
        genreLayout.minimumInteritemSpacing = 8.0
        self.seriesHeaderGenreCollectionView.collectionViewLayout = genreLayout
        self.seriesGenreCollectionView.collectionViewLayout = genreLayout
        
        let castLayout = UICollectionViewFlowLayout()
        castLayout.scrollDirection = .horizontal
        castLayout.itemSize = CGSize(width: 100, height: 140)
        castLayout.minimumInteritemSpacing = 8.0
        self.seriesCastCollectionView.collectionViewLayout = castLayout
        
        let likeLayout = UICollectionViewFlowLayout()
        likeLayout.scrollDirection = .horizontal
        likeLayout.itemSize = CGSize(width: 130, height: 250)
        likeLayout.minimumInteritemSpacing = 8.0
        self.seriesRelatedCollectionView.collectionViewLayout = likeLayout
        
        // Do any additional setup after loading the view.
        let parameters:[String: String] = ["type": type, "id": videoID]
        getDetails(parameter: parameters)
    }
    
    func getDetails(parameter: [String: String]) {
        self.showSpinner(onView: self.view)
        let base = Webservice.details
        
        getDataWithHeaderAndBody(url: base, newBody: parameter) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(TVSeriesDataModel.self, from: data)
                
                let thumb = result.thumbnailURL
                let poster = result.posterURL
                let title = result.title
                let des = result.tvSeriesDataModelDescription
                let release = result.release
                
                for seasonModel in result.season {
                    self.seasonArray.append(seasonModel)
                    
                    for episodeModel in seasonModel.episodes {
                        self.episodeArray.append(episodeModel)
                    }
                }
                
                for genreModel in result.genre {
                    self.seriesGenreArray.append(genreModel)
                }
                
                for directorModel in result.director {
                    self.seriesDirectorArray.append(directorModel)
                }
                
                for castModel in result.cast {
                    self.seriesCastArray.append(castModel)
                }
                
                for relatedModel in result.relatedTvseries {
                    self.seriesRelatedArray.append(relatedModel)
                }
                
                DispatchQueue.main.async {
                    self.removeSpinner()
                    guard let thumbURL = URL(string: thumb) else { return }
                    let thumb = try? Data(contentsOf: thumbURL)
                    
                    if let thumbData = thumb {
                        let image = UIImage(data: thumbData)
                        self.thumbImageView.image = image
                    }
                    
                    guard let posterURL = URL(string: poster) else { return }
                    let poster = try? Data(contentsOf: posterURL)
                    
                    if let posterData = poster {
                        let image = UIImage(data: posterData)
                        self.posterImageView.image = image
                    }
                    
                    self.titleLabel.text = title
                    self.descriptionLabel.text = des
                    self.releaseLabel.text = "Release on: \(release)"
                    
                    //reload collection views
                    self.seriesHeaderGenreCollectionView.reloadData()
                    self.seasonCollectionView.reloadData()
                    self.episodeCollectionView.reloadData()
                    self.seriesDirectorCollectionView.reloadData()
                    self.seriesGenreCollectionView.reloadData()
                    self.seriesCastCollectionView.reloadData()
                    self.seriesRelatedCollectionView.reloadData()
                }
                
            } catch {
                print(error.localizedDescription)
            }
            self.removeSpinner()
        }
    }
}

extension TVSeriesDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.seriesHeaderGenreCollectionView:
            return seriesGenreArray.count
        case self.seasonCollectionView:
            return seasonArray.count
        case self.episodeCollectionView:
            return episodeArray.count
        case self.seriesDirectorCollectionView:
            return seriesDirectorArray.count
        case self.seriesGenreCollectionView:
            return seriesGenreArray.count
        case self.seriesCastCollectionView:
            return seriesCastArray.count
        case self.seriesRelatedCollectionView:
            return seriesRelatedArray.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case self.seriesHeaderGenreCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tvHeaderGenreCell", for: indexPath) as! TVSeriesHeaderGenreViewCell
            cell.genreLabel.text = seriesGenreArray[indexPath.item].name
            return cell
        case self.seasonCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "seasonCell", for: indexPath) as! SeasonViewCell
            cell.seasonTitleLabel.text = seasonArray[indexPath.item].seasonsName
            return cell
        case self.episodeCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "episodeCell", for: indexPath) as! EpisodeViewCell
            cell.episodeTitleLabel.text = seriesRelatedArray[indexPath.item].title
            guard let ImageURL = URL(string: episodeArray[indexPath.item].imageURL) else { return cell }
            let data = try? Data(contentsOf: ImageURL)
            if let imageData = data {
                let image = UIImage(data: imageData)
                cell.episodeImageView.image = image
            }
            return cell
        case self.seriesDirectorCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tvDirectorCell", for: indexPath) as! TVSeriesDirectorViewCell
            cell.directorLabel.text = seriesDirectorArray[indexPath.item].name
            guard let ImageURL = URL(string: seriesDirectorArray[indexPath.item].imageURL) else { return cell }
            let data = try? Data(contentsOf: ImageURL)
            if let imageData = data {
                let image = UIImage(data: imageData)
                cell.directorImageView.image = image
            }
            return cell
        case self.seriesGenreCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tvGenreCell", for: indexPath) as! TVSeriesGenreViewCell
            cell.genreLabel.text = seriesGenreArray[indexPath.item].name
            return cell
        case self.seriesCastCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tvCastCell", for: indexPath) as! TVSeriesCastViewCell
            cell.castLabel.text = seriesCastArray[indexPath.item].name
            guard let ImageURL = URL(string: seriesCastArray[indexPath.item].imageURL) else { return cell }
            let data = try? Data(contentsOf: ImageURL)
            if let imageData = data {
                let image = UIImage(data: imageData)
                cell.castImageView.image = image
            }
            return cell
        case self.seriesRelatedCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "relatedCell", for: indexPath) as! TVSeriesRelatedViewCell
            cell.relatedTitleLabel.text = seriesRelatedArray[indexPath.item].title
            guard let ImageURL = URL(string: seriesRelatedArray[indexPath.item].posterURL) else { return cell }
            let data = try? Data(contentsOf: ImageURL)
            if let imageData = data {
                let image = UIImage(data: imageData)
                cell.relatedImageView.image = image
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.seriesRelatedCollectionView {
            let seriesID = seriesRelatedArray[indexPath.item].videosID
            let categoryView: TVSeriesDetailViewController = self.storyboard?.instantiateViewController(identifier: "TVSeriesDetailViewController") as! TVSeriesDetailViewController
            categoryView.videoID = seriesID
            self.navigationController?.pushViewController(categoryView, animated: true)
        }
    }
}
