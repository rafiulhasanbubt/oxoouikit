//
//  ResetViewController.swift
//  OXOO
//
//  Created by SpaGreen on 4/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class ResetViewController: UIViewController {

    @IBOutlet weak var resetTopview: UIView!
    @IBOutlet weak var resetEmailTF: UITextField!
    @IBOutlet weak var resetButtonView: UIButton!
    
    var restEmail: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        resetEmailTF.text = ""
        // Do any additional setup after loading the view.
        resetTopview.layer.cornerRadius = 10
        resetTopview.layer.borderColor = UIColor.lightGray.cgColor
        resetTopview.layer.borderWidth = 0.2
        // reset button view
        resetButtonView.layer.cornerRadius = 10
        resetButtonView.layer.borderWidth = 0.2
    }
    
    @IBAction func resetButtonTapped(_ sender: Any) {       
        
        if resetEmailTF.text != "" {
            
        } else {
            let alertController = UIAlertController(title: "Reset Alert", message:
                "Email field can't empty", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Try Again", style: .default))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
