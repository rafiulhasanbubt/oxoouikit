//
//  SignupViewController.swift
//  OXOO
//
//  Created by SpaGreen on 4/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    @IBOutlet weak var signupTopview: UIView!
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var emailSignupTF: UITextField!
    @IBOutlet weak var passwordSignupTF: UITextField!
    @IBOutlet weak var signupButtonView: UIButton!
    
    var signupModel: SignupRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullNameTF.text = ""
        emailSignupTF.text = ""
        passwordSignupTF.text = ""
        // Do any additional setup after loading the view.
        signupTopview.layer.cornerRadius = 10
        signupTopview.layer.borderColor = UIColor.lightGray.cgColor
        signupTopview.layer.borderWidth = 0.2
        //Signup button view
        signupButtonView.layer.cornerRadius = 10
        signupButtonView.layer.borderWidth = 0.2
    }
    
    @IBAction func registerButtonTapped(_ sender: UIButton) {
        if (fullNameTF.text != "" && passwordSignupTF.text != "" && emailSignupTF.text != "") {
            self.showSpinner(onView: self.view)
            if (emailSignupTF.text?.isValidEmail())! {
                SignupMethod(fullName: fullNameTF.text!, password: passwordSignupTF.text!, email: emailSignupTF.text!) { (success, error) in
                    if success {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "Signup SuccessFul", message:
                                "", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "ok", style: .default))
                            self.present(alertController, animated: true, completion: nil)
                            self.removeSpinner()
                        }
                    }
                }
                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "signin") as! LoginViewController
                self.navigationController?.pushViewController(homeViewController, animated: true)
            } else {
                let alertController = UIAlertController(title: "Signup Alert", message:
                    "Email address isn't valid", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Enter a valid email", style:.cancel))
                
                self.present(alertController, animated: true, completion: nil)
                self.removeSpinner()
            }
        } else {
            let alertController = UIAlertController(title: "Signup Alert", message:
                "Name, email or password field can't empty", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Try Again", style:.cancel))
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        fullNameTF.text = ""
        emailSignupTF.text = ""
        passwordSignupTF.text = ""
    }
    
    private func SignupMethod(fullName: String, password: String, email: String, completion: @escaping(Bool, Error?) -> Void ) {
        
        if (Reachability.shared.isConnectedToNetwork()) {
            let baseURL = URL(string: WebServices.signup)
            let apikey = WebServices.apikeyParameters
            
            var request = URLRequest(url: baseURL!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let body = SignupRequest(apiSecretKey: apikey, email: email, password: password, name: fullName)
            
            request.httpBody = try! JSONEncoder().encode(body)
            
            let task = URLSession.shared.dataTask(with: baseURL!) { (data, response, error) in
                guard data != nil else {
                    completion(false, error)
                    return
                }
                
                do {
                    let encoder = JSONEncoder()
                    let responseObject = try encoder.encode(body.self)
                    print(responseObject)
                    completion(true, nil)
                } catch {
                    completion(false, error)
                }
            }
            task.resume()
        } else {
            let alertController = UIAlertController(title: "Network Problem", message:
                "Please check your network connection", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "ok", style: .default))
            self.present(alertController, animated: true, completion: nil)
            removeSpinner()
        }
    }
}
