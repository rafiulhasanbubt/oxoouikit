//
//  LoginViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 31/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginTopview: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var signinButtonView: UIButton!
    @IBOutlet weak var guestButtonView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTF.text = ""
        passwordTF.text = ""
        
        // Do any additional setup after loading the view.
        loginTopview.layer.cornerRadius = 10
        loginTopview.layer.borderColor = UIColor.lightGray.cgColor
        loginTopview.layer.borderWidth = 0.2
        // Button view
        signinButtonView.layer.cornerRadius = 10
        signinButtonView.layer.borderWidth = 0.2
        //guest Button
        guestButtonView.layer.cornerRadius = 10
        guestButtonView.layer.borderWidth = 0.2
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if (emailTF.text != "" && passwordTF.text != "") {
            self.showSpinner(onView: self.view)
            loginMethod(userName: emailTF.text!, password: passwordTF.text!) { (success, error) in
                self.removeSpinner()
                if success {
                    UserDefaults.standard.set(true, forKey: "loggedIn")
                    DispatchQueue.main.async {
                        let alertController = UIAlertController(title: "Sigin SuccessFul", message:
                            "", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "ok", style: .default))
                        self.present(alertController, animated: true, completion: nil)
                    }
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! MainTabBarController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }
        } else {
            let alertController = UIAlertController(title: "Login Alert", message:
                "Name or password field can't empty", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Try Again", style:.cancel))
            
            self.present(alertController, animated: true, completion: nil)
        }
        emailTF.text = ""
        passwordTF.text = ""
    }
    
    
    @IBAction func SignupButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func forgotButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func guestButtonPressed(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! MainTabBarController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    private func loginMethod(userName: String, password: String, completion: @escaping(Bool, Error?) -> Void ) {
        
        if (Reachability.shared.isConnectedToNetwork()) {
            let baseURL = URL(string: WebServices.login)
            let apikey = WebServices.apikeyParameters
            let body = loginRequest (email: userName, password: password, requestToken: apikey)
            
            var request = URLRequest(url: baseURL!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = try! JSONEncoder().encode(body)
            
            let task = URLSession.shared.dataTask(with: baseURL!) { (data, response, error) in
                guard data != nil else {
                    completion(false, error)
                    return
                }
                
                do {
                    let encoder = JSONEncoder()
                    let responseObject = try encoder.encode(body.self)
                    DispatchQueue.main.async {
                        print(responseObject)
                        completion(true, nil)
                    }
                    
                } catch {
                    completion(false, error)
                }
            }
            task.resume()
        } else {
            let alertController = UIAlertController(title: "Network Problem", message:
                "Please check your network connection", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "ok", style: .default))
            self.present(alertController, animated: true, completion: nil)
            removeSpinner()
        }
    }
}

