//
//  MovieDetailViewController.swift
//  OXOO
//
//  Created by SpaGreen on 26/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    var movieDataArray = [MovieDetailsDataModel]()
    
    @IBOutlet weak var headerGenreCollectionView: UICollectionView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!   
    @IBOutlet weak var releaseLabel: UILabel!
    
    @IBOutlet weak var directoreCollectionView: UICollectionView!
    @IBOutlet weak var genreCollectionView: UICollectionView!
    @IBOutlet weak var castCollectionView: UICollectionView!
    @IBOutlet weak var downloadCollectionView: UICollectionView!
    @IBOutlet weak var likeCollectionView: UICollectionView!
    
    var videoID: String = ""
    var type: String = "movie"
    
    var videoArray = [Video]()
    var directorArray = [Cast]()
    var castArray = [Cast]()
    var downloadArray = [DownloadLink]()
    var genreArray = [Genre]()
    var likeArray = [RelatedMovie]()
    
    @IBOutlet weak var movieDetailTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        headerGenreCollectionView.delegate = self
        headerGenreCollectionView.dataSource = self
        directoreCollectionView.delegate = self
        directoreCollectionView.dataSource = self
        genreCollectionView.delegate = self
        genreCollectionView.dataSource = self
        castCollectionView.delegate = self
        castCollectionView.dataSource = self
        downloadCollectionView.delegate = self
        downloadCollectionView.dataSource = self
        likeCollectionView.delegate = self
        likeCollectionView.dataSource = self
        
        let directorLayout = UICollectionViewFlowLayout()
        directorLayout.scrollDirection = .horizontal
        directorLayout.itemSize = CGSize(width: 100, height: 140)
        directorLayout.minimumInteritemSpacing = 8.0
        self.directoreCollectionView.collectionViewLayout = directorLayout
        
        let genreLayout = UICollectionViewFlowLayout()
        genreLayout.scrollDirection = .horizontal
        genreLayout.itemSize = CGSize(width: 80, height: 44)
        genreLayout.minimumInteritemSpacing = 8.0
        self.headerGenreCollectionView.collectionViewLayout = genreLayout
        self.genreCollectionView.collectionViewLayout = genreLayout
        
        let castLayout = UICollectionViewFlowLayout()
        castLayout.scrollDirection = .horizontal
        castLayout.itemSize = CGSize(width: 100, height: 140)
        castLayout.minimumInteritemSpacing = 8.0
        self.castCollectionView.collectionViewLayout = castLayout
        
        let downloadLayout = UICollectionViewFlowLayout()
        downloadLayout.scrollDirection = .horizontal
        downloadLayout.itemSize = CGSize(width: 100, height: 100)
        downloadLayout.minimumInteritemSpacing = 8.0
        self.downloadCollectionView.collectionViewLayout = downloadLayout
        
        let likeLayout = UICollectionViewFlowLayout()
        likeLayout.scrollDirection = .horizontal
        likeLayout.itemSize = CGSize(width: 130, height: 250)
        likeLayout.minimumInteritemSpacing = 8.0
        self.likeCollectionView.collectionViewLayout = likeLayout
        
        let parameters: [String: String] = ["type": type, "id": videoID]
        getDetails(parameter: parameters)
    }
    
    @IBAction func watchButtonPressed(_ sender: UIButton) {
        //PlayerViewController
        let categoryView: PlayerViewController = self.storyboard?.instantiateViewController(identifier: "PlayerViewController") as! PlayerViewController
        //categoryView.videoID = movieID
        categoryView.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(categoryView, animated: true)
    }
    
    //MARK: get data
    func getDetails(parameter: [String: String]) {
        self.showSpinner(onView: self.view)
        let base = Webservice.details
        
        getDataWithHeaderAndBody(url: base, newBody: parameter) { (response) in
            let data = response
            
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(MovieDetailsDataModel.self, from: data)
                
                let thumb = result.thumbnailURL
                let poster = result.posterURL
                let title = result.title
                let des = result.movieDescription
                let release = result.release
                
                for videoModel in result.videos {
                    self.videoArray.append(videoModel)
                }
                
                for directorModel in result.director {
                    self.directorArray.append(directorModel)
                }
                
                for genreModel in result.genre {
                    self.genreArray.append(genreModel)
                }
                
                for castModel in result.castAndCrew {
                    self.castArray.append(castModel)
                }
                
                for downloadModel in result.downloadLinks {
                    self.downloadArray.append(downloadModel)
                }
                for likeModel in result.relatedMovie {
                    self.likeArray.append(likeModel)
                }
                
                DispatchQueue.main.async {
                    self.removeSpinner()
                    guard let thumbURL = URL(string: thumb) else { return }
                    let thumb = try? Data(contentsOf: thumbURL)
                    
                    if let thumbData = thumb {
                        let image = UIImage(data: thumbData)
                        self.thumbImageView.image = image
                    }
                    
                    guard let posterURL = URL(string: poster) else { return }
                    let poster = try? Data(contentsOf: posterURL)
                    
                    if let posterData = poster {
                        let image = UIImage(data: posterData)
                        self.posterImageView.image = image
                    }
                    
                    self.titleLabel.text = title
                    self.descriptionLabel.text = des
                    self.releaseLabel.text = "Release on: \(release)"
                    
                    //reload collection views
                    self.headerGenreCollectionView.reloadData()
                    self.directoreCollectionView.reloadData()
                    self.genreCollectionView.reloadData()
                    self.castCollectionView.reloadData()
                    self.downloadCollectionView.reloadData()
                    self.likeCollectionView.reloadData()
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}


extension MovieDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.headerGenreCollectionView:
            return genreArray.count
        case self.directoreCollectionView:
            return directorArray.count
        case self.genreCollectionView:
            return genreArray.count
        case self.castCollectionView:
            return castArray.count
        case self.downloadCollectionView:
            return downloadArray.count
        case self.likeCollectionView:
            return likeArray.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case self.headerGenreCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerGenreCell", for: indexPath) as! HeaderGenreCollectionViewCell
            cell.headerGenreLabel.text = genreArray[indexPath.row].name
            return cell
        case self.directoreCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "directorCell", for: indexPath) as! DirectorCollectionViewCell
            cell.directorLabel.text = directorArray[indexPath.row].name
            guard let ImageURL = URL(string: directorArray[indexPath.row].imageURL) else { return cell }
            let data = try? Data(contentsOf: ImageURL)
            if let imageData = data {
                let image = UIImage(data: imageData)
                cell.directorImageView.image = image
            }
            return cell
        case self.genreCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genreCell", for: indexPath) as! GenreCollectionViewCell
            cell.genreLabel.text = genreArray[indexPath.row].name
            return cell
        case self.castCollectionView:
            let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "castCell", for: indexPath) as! CastCollectionViewCell)
            cell.castNameLabel.text = castArray[indexPath.row].name
            cell.castImageView.image = UIImage(named: "user")
            guard let ImageURL = URL(string: castArray[indexPath.row].imageURL) else { return cell }
            let data = try? Data(contentsOf: ImageURL)
            if let imageData = data {
                let image = UIImage(data: imageData)
                cell.castImageView.image = image
            }
            return cell
        case self.downloadCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "downloadCell", for: indexPath) as! DownloadCollectionViewCell
            cell.downloadSourceLabel.text = downloadArray[indexPath.row].label
            cell.downloadSizeLabel.text = downloadArray[indexPath.row].fileSize
            cell.downloadQualityLabel.text = downloadArray[indexPath.row].resolution
            return cell
        case self.likeCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "likeCell", for: indexPath) as! LikeCollectionViewCell
            cell.likeTitleLabel.text = likeArray[indexPath.row].title
            cell.likeReleaseLabel.text = likeArray[indexPath.row].release
            guard let ImageURL = URL(string: likeArray[indexPath.row].posterURL) else { return cell }
            let data = try? Data(contentsOf: ImageURL)
            if let imageData = data {
                let image = UIImage(data: imageData)
                cell.likeImageView.image = image
            }
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.likeCollectionView {
            let movieID = likeArray[indexPath.row].videosID
            //print("movie id: ",movieID)
            let categoryView: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
            categoryView.videoID = movieID
            self.navigationController?.pushViewController(categoryView, animated: true)
        }
    }
}
