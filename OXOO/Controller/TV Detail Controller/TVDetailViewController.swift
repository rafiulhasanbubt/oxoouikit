//
//  TVDetailViewController.swift
//  OXOO
//
//  Created by SpaGreen on 23/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class TVDetailViewController: UIViewController {
    
    @IBOutlet weak var liveTvView: liveView!
    @IBOutlet weak var streamView: StreamView!
    @IBOutlet weak var tvDetailView: TVDetailView!
    
    @IBOutlet weak var allTVCollectionView: UICollectionView!
    
    var tvID: String!
    var type = "tv"
    var channelDetail = [TVDetailDataModel]()
    var allChannelArray = [ChannelDataModel]()
    
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    var controller = AVPlayerViewController()
    
    var isVideoPlaying = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allTVCollectionView.delegate = self
        allTVCollectionView.dataSource = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 140, height: 110)
        flowLayout.minimumInteritemSpacing = 8.0
        self.allTVCollectionView.collectionViewLayout = flowLayout
        // Do any additional setup after loading the view.
        
        guard let ids = tvID else { return }
        
        let parameters: [String: String] = ["type":type,"id":ids]
        getDetails(parameter: parameters)
    }
    
    func getDetails(parameter: [String: String]) {
        self.showSpinner(onView: self.view)
        let base = Webservice.details
        
        getDataWithHeaderAndBody(url: base, newBody: parameter) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(TVDetailDataModel.self, from: data)
                
                let name = result.tvName
                let streamLabel = result.streamLabel
                let posterURL = result.posterURL
                let streamUrl = result.streamURL
                
                for channelModel in result.allTvChannel! {
                    self.allChannelArray.append(channelModel)
                }
                
                DispatchQueue.main.async {
                    self.tvDetailView.nameLabel.text = name
                    self.streamView.streamLabel.text = streamLabel
                    
                    let ImageURL = URL(string: posterURL)
                    let data = try? Data(contentsOf: ImageURL!)
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        self.tvDetailView.tvImageView.image = image
                    }
                    
                    self.playVideo(streamURL: streamUrl)
                    self.allTVCollectionView.reloadData()
                }
               
            } catch {
                print(error.localizedDescription)
            }
            self.removeSpinner()
        }
    }
    
    //MARK: Video player
    private func playVideo(streamURL: String) {
        let videoURL =  URL(string: streamURL)
        let player = AVPlayer(url: videoURL!)
        controller = AVPlayerViewController()
        self.addChild(controller)
        self.liveTvView.videoView?.addSubview(controller.view)
        controller.view.frame = CGRect(x: 0, y: 0, width: self.liveTvView.videoView!.frame.size.width, height: self.liveTvView.videoView!.frame.size.height)
        controller.player = player
        controller.showsPlaybackControls = true
        player.pause()
        player.play()
    }
}

extension TVDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allChannelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "detailChannelCell", for: indexPath) as? ChannelViewCell
        
        cell!.channelNameLabel.text = allChannelArray[indexPath.row].tvName
        cell!.channelImageView.image = UIImage(named: "dummy TV")
        
        guard let ImageURL = URL(string: allChannelArray[indexPath.row].posterURL) else { return cell! }
        let data = try? Data(contentsOf: ImageURL)

        if let imageData = data {
            let image = UIImage(data: imageData)
            cell!.channelImageView.image = image
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let channelID = allChannelArray[indexPath.row].liveTvID
        let channelView: TVDetailViewController = self.storyboard?.instantiateViewController(identifier: "TVDetailViewController") as! TVDetailViewController
        channelView.tvID = channelID
        self.navigationController?.pushViewController(channelView, animated: true)
    }
}
