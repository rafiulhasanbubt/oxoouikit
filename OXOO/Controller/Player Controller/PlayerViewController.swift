//
//  PlayerViewController.swift
//  OXOO
//
//  Created by SpaGreen on 4/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class PlayerViewController: UIViewController {

    //View outlet
    @IBOutlet weak var playerView: UIView!
    
    //Player instances
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    var controller = AVPlayerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    private func playVideo(streamURL: String) {
        let videoURL =  URL(string: streamURL)
        let player = AVPlayer(url: videoURL!)
        controller = AVPlayerViewController()
        self.addChild(controller)
        self.playerView?.addSubview(controller.view)
        controller.view.frame = CGRect(x: 0, y: 0, width: self.playerView!.frame.size.width, height: self.playerView!.frame.size.height)
        controller.player = player
        controller.showsPlaybackControls = true
        player.pause()
        player.play()
    }
}
