//
//  ViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 16/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController {
    
    //Array to display menu options
    @IBOutlet var slideMenuTableView : UITableView!
    
    //Transparent button to hide menu
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    //Array containing menu options
    var menuItemArray = [Dictionary<String,String>]() 
    
    //Menu button which was tapped to display the menu
    var buttonMenu : UIButton!
    
    //Delegate of the MenuVC
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slideMenuTableView.dataSource = self
        slideMenuTableView.delegate = self
        slideMenuTableView.separatorStyle = .none
        // Do any additional setup after loading the view.
        
        let menuNib = UINib(nibName: "SlideMenuTableViewCell", bundle: nil)
        slideMenuTableView.register(menuNib, forCellReuseIdentifier: "slideMenuCell")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        let isLogin = UserDefaults.standard.bool(forKey: "loggedIn")
        if(isLogin) {
            menuItemArray.append(["title":"Home", "icon":"home"])
            menuItemArray.append(["title":"Movies", "icon":"movie"])
            menuItemArray.append(["title":"TV Series", "icon":"moviefilm"])
            menuItemArray.append(["title":"Live TV", "icon":"livetv"])
            menuItemArray.append(["title":"Genre", "icon":"genre"])
            menuItemArray.append(["title":"Country", "icon":"country"])
            menuItemArray.append(["title":"Profile", "icon":"user"])
            menuItemArray.append(["title":"Favorite", "icon":"favorite"])
            menuItemArray.append(["title":"Subscription", "icon":"subscription"])
            menuItemArray.append(["title":"Download", "icon":"download"])
            menuItemArray.append(["title":"Setting", "icon":"settings"])
            menuItemArray.append(["title":"Sign out", "icon":"lock"])
            slideMenuTableView.reloadData()
        } else {
            menuItemArray.append(["title":"Home", "icon":"home"])
            menuItemArray.append(["title":"Movies", "icon":"movie"])
            menuItemArray.append(["title":"TV Series", "icon":"moviefilm"])
            menuItemArray.append(["title":"Live TV", "icon":"livetv"])
            menuItemArray.append(["title":"Genre", "icon":"genre"])
            menuItemArray.append(["title":"Country", "icon":"country"])
            menuItemArray.append(["title":"Login", "icon":"lock"])
            slideMenuTableView.reloadData()
        }
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        buttonMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "slideMenuCell", for: indexPath) as! SlideMenuTableViewCell
        
        cell.menuNameLabel.text = menuItemArray[indexPath.row]["title"]!
        cell.menuImageView?.image = UIImage(named: menuItemArray[indexPath.row]["icon"]!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
        
    }
    
}
