//
//  HomeViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 24/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barStyle = .black
    }

}
