//
//  ViewController.swift
//  OXOO
//
//  Created by Abdul Mannan on 16/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    
    @IBOutlet weak var homeTableView: UITableView! {
        didSet {
            homeTableView.register(UINib(nibName: "SliderTableViewCell", bundle: Bundle(for: SliderTableViewCell.self)), forCellReuseIdentifier: "sliderCell")
            
            homeTableView.register(UINib(nibName: "GenreTableViewCell", bundle: Bundle(for: GenreTableViewCell.self)), forCellReuseIdentifier: "genreCell")
            
            homeTableView.register(UINib(nibName: "CountryTableViewCell", bundle: Bundle(for: CountryTableViewCell.self)), forCellReuseIdentifier: "countryCell")
        
            homeTableView.register(UINib(nibName:"TVChannelTableViewCell", bundle: Bundle(for: TVChannelTableViewCell.self)), forCellReuseIdentifier: "channelCell")
            
            homeTableView.register(UINib(nibName: "LatestMovieTableViewCell", bundle: Bundle(for: LatestMovieTableViewCell.self)), forCellReuseIdentifier: "latestMovieCell")
            
            homeTableView.register(UINib(nibName: "LatestTVSeriesTableViewCell", bundle: Bundle(for: LatestTVSeriesTableViewCell.self)), forCellReuseIdentifier: "tvSeriesCell")
            
            homeTableView.register(UINib(nibName: "ActionTableViewCell", bundle: Bundle(for: ActionTableViewCell.self)), forCellReuseIdentifier: "categoryCell")
            
            homeTableView.register(UINib(nibName: "ComedyTableViewCell", bundle: Bundle(for: ComedyTableViewCell.self)), forCellReuseIdentifier: "comedyCell")
            
            homeTableView.register(UINib(nibName: "HorrorTableViewCell", bundle: Bundle(for: HorrorTableViewCell.self)), forCellReuseIdentifier: "horrorCell")
            
            homeTableView.register(UINib(nibName: "ThrillerTableViewCell", bundle: Bundle(for: ThrillerTableViewCell.self)), forCellReuseIdentifier: "thrillerCell")
            
            homeTableView.rowHeight = UITableView.automaticDimension
            homeTableView.estimatedRowHeight = 300
            homeTableView.separatorStyle = .none
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSpinner(onView: view)
        addSlideMenuButton()
        // Do any additional setup after loading the view.
        homeTableView.dataSource = self
        homeTableView.delegate = self
        
        func randomColor() -> UIColor {
            let redValue = CGFloat(drand48())
            let greenValue = CGFloat(drand48())
            let blueValue = CGFloat(drand48())
            return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: 1.0)
        }
        removeSpinner()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barStyle = .black
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource, SliderCellDelegate, TVCellDelegate, GenreCellDelegate, CountryCellDelegate, LatestCellDelegate, LatestTVCellDelegate, ActionCellDelegate, ComedyCellDelegate, HorrorCellDelegate, ThrillerCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! SliderTableViewCell
            cell.sliderDelegate = self
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath) as! GenreTableViewCell
            cell.genreDelegate = self
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath) as! CountryTableViewCell
            cell.countryDelegate = self
            return cell
        }
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "channelCell", for: indexPath) as! TVChannelTableViewCell
            cell.tvDelegate = self
            return cell
        }
        else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "latestMovieCell", for: indexPath) as! LatestMovieTableViewCell
            cell.latestDelegate = self
            return cell
        }
        else if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "tvSeriesCell", for: indexPath) as! LatestTVSeriesTableViewCell
            cell.latestTVDelegate = self
            return cell
        }
        else if indexPath.row == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! ActionTableViewCell
            cell.actionDelegate = self
            return cell
        }
        else if indexPath.row == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "comedyCell", for: indexPath) as! ComedyTableViewCell
            cell.codemyDelegate = self
            return cell
        }
        else if indexPath.row == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "horrorCell", for: indexPath) as! HorrorTableViewCell
            cell.horrorDelegate = self
            return cell
        }
        else if indexPath.row == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "thrillerCell", for: indexPath) as! ThrillerTableViewCell
            cell.thrillerDelegate = self
            return cell
        }
        
        return UITableViewCell()
    }
    
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: SliderTableViewCell, sliderID: String) {
        let detailController: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        detailController.videoID = sliderID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectionCell: CategoryCollectionViewCell?, didTappedInTableview TableCell: GenreTableViewCell, genreID: String) {
        let detailController: GenreCategoryViewController = self.storyboard?.instantiateViewController(identifier: "CategoryViewController") as! GenreCategoryViewController
        detailController.genreId = genreID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectioncell: ChannelCollectionViewCell?, didTappedInTableview TableCell: CountryTableViewCell, countryID: String) {
        let detailController: CountryCategoryViewController = self.storyboard?.instantiateViewController(identifier: "CountryCategoryViewController") as! CountryCategoryViewController
        detailController.countryId = countryID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectioncell: ChannelCollectionViewCell?, didTappedInTableview TableCell: TVChannelTableViewCell, detailID: String) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailController = storyBoard.instantiateViewController(withIdentifier:"TVDetailViewController") as! TVDetailViewController
        detailController.tvID = detailID
        detailController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: LatestMovieTableViewCell, videoID: String) {
        let detailController: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        detailController.videoID = videoID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: LatestTVSeriesTableViewCell, videoID: String) {
        let detailController: TVSeriesDetailViewController = self.storyboard?.instantiateViewController(identifier: "TVSeriesDetailViewController") as! TVSeriesDetailViewController
        detailController.videoID = videoID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: ActionTableViewCell, videoID: String) {
        let detailController: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        detailController.videoID = videoID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: ComedyTableViewCell, videoID: String) {
        let detailController: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        detailController.videoID = videoID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: HorrorTableViewCell, videoID: String) {
        let detailController: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        detailController.videoID = videoID
        self.navigationController?.pushViewController(detailController, animated: true)
    }

    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: ThrillerTableViewCell, videoID: String) {
        let detailController: MovieDetailViewController = self.storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        detailController.videoID = videoID
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
}
