//
//  WebServiceEndpoint.swift
//  OXOO
//
//  Created by SpaGreen on 1/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation
struct AppContainer {
    let name: String
    let email: String
    let pass: String
}

class WebserviceEndpoint {
    func makeRequest(endpoint: String, parameters: [String: String], completionHandler: @escaping (AppContainer?, Error?) -> ()) {
        
        guard var urlComponents = URLComponents(string: endpoint) else {
            print("Invalid endpoint")
            return
        }
        
        // Build an array containing the parameters the user specified
        var queryItems = parameters.map { key, value in URLQueryItem(name: key, value: value) }
        
        // Optional: Add default values for parameters that the user missed
        if !queryItems.contains(where: { $0.name == "token" }) {
            queryItems.append(URLQueryItem(name: "token", value: "123"))
        }
        
        // Add these parameters to the URLComponents
        urlComponents.queryItems = queryItems
        
        // And here's your final URL
        guard let url = urlComponents.url else {
            print("Cannot construct URL")
            return
        }
        
        print(url)
        // ... rest of your function
    }
    
}
