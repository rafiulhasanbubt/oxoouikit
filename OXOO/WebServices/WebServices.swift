//
//  WebServices.swift
//  OXOO
//
//  Created by Abdul Mannan on 21/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

class WebServices {
    //baseurl
    static let baseUrl = "http://demo.redtvlive.com/oxoo/v110/api/"
    //apisecretkey
    static let apiSecretKey = "wv0e0d0jsl78pwgo63h7fhd9"
    
    //apikey parameters
    static let apikeyParameters = "api_secret_key=\(apiSecretKey)"
    static let termsUrl = "http://demo.redtvlive.com/oxoo/v110/terms/"
    
    //Signup/login APIs
    static let signup = "\(baseUrl)signup?"
    static let login = "\(baseUrl)login?"
    
}
