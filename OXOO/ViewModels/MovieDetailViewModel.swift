//
//  MovieDetailViewModel.swift
//  OXOO
//
//  Created by SpaGreen on 27/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation
import UIKit

enum MovieDetailViewModelType {
    case title
    case posterURL
    case thumbnailURL
    case genre
    case movieDescription
    case release
    case director
    case writer
    case videos
    case cast
    case relatedMovie
}

protocol MovieDetailViewModel {
    var type: MovieDetailViewModelType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}

