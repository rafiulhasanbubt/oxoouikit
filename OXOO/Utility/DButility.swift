//
//  DButility.swift
//  OXOO
//
//  Created by SpaGreen on 9/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

class DButility: NSObject {
    
    class func getPath(_ fileName: String) -> String {
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentDirectory.appendingPathComponent(fileName)
        //print(fileURL.path)
        return fileURL.path
    }
    
    class func copyDatabase(_ filename: String) {
        let dbPath = getPath("Signup.db")
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: dbPath) {
            let bundle = Bundle.main.resourceURL
            let file = bundle?.appendingPathComponent(filename)
            // if error occurs
            var error: NSError?
            
            do {
                try fileManager.copyItem(atPath: (file?.path)!, toPath: dbPath)
            } catch let err as NSError {
                error = err
            }
            // message send from here
            if error == nil {
                print("error in database")
            } else {
                print("good")
            }
            
        }
    }
}
