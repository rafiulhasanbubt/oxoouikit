//
//  DatabaseManager.swift
//  OXOO
//
//  Created by SpaGreen on 9/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

var shareInstance = DatabaseManager()

class DatabaseManager: NSObject {
    var database: FMDatabase? = nil
    class func getInstance() -> DatabaseManager {
        if shareInstance.database == nil {
            shareInstance.database = FMDatabase(path: DButility.getPath("Signup.db"))
        }
        return shareInstance
    }
    
    ///
    func saveData(_ modelInfo: SignupRequest) -> Bool {
        shareInstance.database?.open()
        
        let isSave = shareInstance.database?.executeUpdate("INSERT INTO Signup (fullName, email, password) VALUES (?,?,?)", withArgumentsIn: [modelInfo.name, modelInfo.email, modelInfo.password])
        
        shareInstance.database?.close()        
        return isSave!
    }
}
