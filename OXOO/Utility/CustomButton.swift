//
//  CustomButton.swift
//  OXOO
//
//  Created by SpaGreen on 8/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//
import Foundation
import UIKit

class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    func setupButton() {
        setShadow()
        setTitleColor(.white, for: .normal)
        
        //backgroundColor      = Colors.coolBlue
        //titleLabel?.font     = UIFont(name: "helvetica neue", size: 18)
        layer.cornerRadius   = 12
        layer.borderWidth    = 0.0
        layer.borderColor    = UIColor.darkGray.cgColor
    }
    
    private func setShadow() {
        layer.shadowColor   = UIColor.lightGray.cgColor
        layer.shadowOffset  = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius  = 2
        layer.shadowOpacity = 0.5
        clipsToBounds       = true
        layer.masksToBounds = false
    }
    
}
