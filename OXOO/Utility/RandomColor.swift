//
//  RandomColor.swift
//  OXOO
//
//  Created by Abdul Mannan on 23/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation
import  UIKit


class RandomColor {
    func randomColor() -> UIColor {
        let redValue = CGFloat(drand48())
        let greenValue = CGFloat(drand48())
        let blueValue = CGFloat(drand48())
        
        return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: 1.0)
    }
}
