//
//  ColorExtension.swift
//  OXOO
//
//  Created by Abdul Mannan on 22/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation
import UIKit

extension Array {
    func randomItem() -> Element? {
        if isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}

let redColor = UIColor(red:0.94, green:0.33, blue:0.31, alpha:1.0)
let indigoColor = UIColor(red:0.36, green:0.42, blue:0.75, alpha:1.0)
let lightGreenColor = UIColor(red:0.61, green:0.80, blue:0.40, alpha:1.0)
let blue400Color = UIColor(red:0.26, green:0.65, blue:0.96, alpha:1.0)
let orange400Color = UIColor(red:1.00, green:0.65, blue:0.15, alpha:1.0)
let blueGray400Color = UIColor(red:0.47, green:0.56, blue:0.61, alpha:1.0)

extension UIColor {
    // primaryColors
    static let primaryColor = UIColor(red:0.40, green:0.45, blue:0.90, alpha:1.0)
    static let primaryDark = UIColor(red:0.33, green:0.35, blue:0.75, alpha:1.0)
    static let accentColor = UIColor(red:0.40, green:0.45, blue:0.90, alpha:1.0)
    //paypalColor
    static let paypalColor = UIColor(red:1.00, green:0.77, blue:0.23, alpha:1.0)
    static let paypalColorDark = UIColor(red:0.83, green:0.60, blue:0.09, alpha:1.0)
    static let stripeColor = UIColor(red:1.00, green:0.77, blue:0.23, alpha:1.0)
    static let raveColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
    static let ravColorLight = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
    //textColor
    static let white = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
    static let default_text = UIColor(red:0.55, green:0.55, blue:0.55, alpha:1.0)
    static let black = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
    static let nav_bg = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    static let nav_head_bg = UIColor(red:0.13, green:0.13, blue:0.13, alpha:1.0)
    //textColor
    static let dark = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
    static let black_1_transarent = UIColor(red:0.55, green:0.55, blue:0.55, alpha:1.0)
    static let black_transparent = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
    static let grey_transparent = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    //overlay color
    static let overlay_dark_10 = UIColor(red:0.13, green:0.13, blue:0.13, alpha:1.0)
    static let overlay_dark_20 = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
    static let overlay_dark_30 = UIColor(red:0.55, green:0.55, blue:0.55, alpha:1.0)
    static let overlay_dark_40 = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
    static let overlay_dark_90 = UIColor(red:0.26, green:0.26, blue:0.26, alpha:1.0)
    //grey color
    static let grey_5 = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
    static let grey_20 = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0)
    static let grey_40 = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0)
    static let grey_60 = UIColor(red:0.40, green:0.40, blue:0.40, alpha:1.0)
    static let grey_90 = UIColor(red:0.15, green:0.20, blue:0.22, alpha:1.0)
    //
    static let red_400 = UIColor(red:0.94, green:0.33, blue:0.31, alpha:1.0)
    static let red_600 = UIColor(red:0.90, green:0.22, blue:0.21, alpha:1.0)
    static let light_blue_300 = UIColor(red:0.31, green:0.76, blue:0.97, alpha:1.0)
    static let green_500 = UIColor(red:0.30, green:0.69, blue:0.31, alpha:1.0)
    static let teal_500 = UIColor(red:0.00, green:0.59, blue:0.53, alpha:1.0)
    static let teal_600 = UIColor(red:0.00, green:0.54, blue:0.48, alpha:1.0)
    static let teal_700 = UIColor(red:0.00, green:0.47, blue:0.42, alpha:1.0)
    // deep_orange_400
    static let deep_orange_400 = UIColor(red:1.00, green:0.44, blue:0.26, alpha:1.0)
    static let blue_grey_300 = UIColor(red:0.56, green:0.64, blue:0.68, alpha:1.0)
    static let blue_grey_400 = UIColor(red:0.47, green:0.56, blue:0.61, alpha:1.0)
    static let indigo_400 = UIColor(red:0.36, green:0.42, blue:0.75, alpha:1.0)
    static let light_green_400 = UIColor(red:0.61, green:0.80, blue:0.40, alpha:1.0)
    //blue_400
    static let blue_400 = UIColor(red:0.26, green:0.65, blue:0.96, alpha:1.0)
    static let orange_400 = UIColor(red:1.00, green:0.65, blue:0.15, alpha:1.0)
    
    static let grey_300 = UIColor(red:0.88, green:0.88, blue:0.88, alpha:1.0)
    static let grey_400 = UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0)
    static let grey_500 = UIColor(red:0.62, green:0.62, blue:0.62, alpha:1.0)
    //
    static let black_overlay = UIColor(red:0.40, green:0.00, blue:0.00, alpha:1.0)
    
    // Create a UIColor from RGB
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    // Create a UIColor from a hex value (E.g 0x000000)
    convenience init(hex: Int, a: CGFloat = 1.0) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF,
            a: a
        )
    }
    
    //RandomColor created
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
