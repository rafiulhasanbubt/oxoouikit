//
//  SliderDataModel.swift
//  OXOO
//
//  Created by Abdul Mannan on 23/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - SliderDataModel
struct SliderDataModel: Codable {
    let sliderType: String
    let data: [SliderData]

    enum CodingKeys: String, CodingKey {
        case sliderType = "slider_type"
        case data
    }
}

// MARK: - Datum
struct SliderData: Codable {
    let sliderID: String
    let title: String
    let datumDescription: String
    let videoLink: String
    let imageLink: String
    let slug: String
    let publication: String

    enum CodingKeys: String, CodingKey {
        case sliderID = "slider_id"
        case title
        case datumDescription = "description"
        case videoLink = "video_link"
        case imageLink = "image_link"
        case slug, publication
    }
}
