//
//  TVSeriesDataModel.swift
//  OXOO
//
//  Created by SpaGreen on 30/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - TVSeriesDataModel
struct TVSeriesDataModel: Codable {
    let videosID, title, tvSeriesDataModelDescription, slug: String
    let release, runtime: String
    let videoQuality: VideoQuality
    let enableDownload: String
    let thumbnailURL, posterURL: String
    let genre: [Genre]
    let country: [Country]
    let director, writer, cast, castAndCrew: [Cast]
    let season: [Season]
    let relatedTvseries: [RelatedTvsery]

    enum CodingKeys: String, CodingKey {
        case videosID = "videos_id"
        case title
        case tvSeriesDataModelDescription = "description"
        case slug, release, runtime
        case videoQuality = "video_quality"
        case enableDownload = "enable_download"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
        case genre, country, director, writer, cast
        case castAndCrew = "cast_and_crew"
        case season
        case relatedTvseries = "related_tvseries"
    }
}

// MARK: - RelatedTvsery
struct RelatedTvsery: Codable {
    let videosID, genre, country, title: String
    let relatedTvseryDescription, slug, release, runtime: String
    let videoQuality: VideoQuality
    let thumbnailURL, posterURL: String

    enum CodingKeys: String, CodingKey {
        case videosID = "videos_id"
        case genre, country, title
        case relatedTvseryDescription = "description"
        case slug, release, runtime
        case videoQuality = "video_quality"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}

enum VideoQuality: String, Codable {
    case hd = "HD"
}

// MARK: - Season
struct Season: Codable {
    let seasonsID, seasonsName: String
    let episodes: [Episode]

    enum CodingKeys: String, CodingKey {
        case seasonsID = "seasons_id"
        case seasonsName = "seasons_name"
        case episodes
    }
}

// MARK: - Episode
struct Episode: Codable {
    let episodesID, episodesName, streamKey, fileType: String
    let imageURL: String
    let fileURL: String
    let subtitle: [String]

    enum CodingKeys: String, CodingKey {
        case episodesID = "episodes_id"
        case episodesName = "episodes_name"
        case streamKey = "stream_key"
        case fileType = "file_type"
        case imageURL = "image_url"
        case fileURL = "file_url"
        case subtitle
    }
}
