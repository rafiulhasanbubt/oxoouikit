//
//  MovieDataModel.swift
//  OXOO
//
//  Created by Abdul Mannan on 21/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - MovieDataModel
struct MovieDataModel: Codable {
    let videosID: String
    let title: String
    let latestDescription: String
    let slug: String
    let release: String
    let runtime: String
    let videoQuality: String
    let thumbnailURL: String
    let posterURL: String

    enum CodingKeys: String, CodingKey {
        case videosID = "videos_id"
        case title
        case latestDescription = "description"
        case slug, release, runtime
        case videoQuality = "video_quality"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}
