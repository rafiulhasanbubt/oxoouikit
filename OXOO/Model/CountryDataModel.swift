//
//  CountryDataModel.swift
//  OXOO
//
//  Created by Abdul Mannan on 21/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - CountryDataModel
struct CountryDataModel: Codable {
    let countryID: String
    let name: String
    let countryDescription: String
    let slug: String
    let url: String
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case countryID = "country_id"
        case name
        case countryDescription = "description"
        case slug
        case url
        case imageURL = "image_url"
    }
}
