//
//  TVDetailsDataModel.swift
//  OXOO
//
//  Created by Abdul Mannan on 21/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - TVDetailDataModel
struct TVDetailDataModel: Codable {
    let liveTvID: String
    let tvName: String
    let detaildescription: String
    let slug: String
    let streamFrom: String
    let streamLabel: String
    let streamURL: String
    let thumbnailURL: String
    let posterURL: String
    let additionalMediaSource: [AdditionalMediaSource]?
    let allTvChannel: [ChannelDataModel]?

    enum CodingKeys: String, CodingKey {
        case liveTvID = "live_tv_id"
        case tvName = "tv_name"
        case detaildescription = "description"
        case slug
        case streamFrom = "stream_from"
        case streamLabel = "stream_label"
        case streamURL = "stream_url"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
        case additionalMediaSource = "additional_media_source"
        case allTvChannel = "all_tv_channel"
    }
}

// MARK: - AdditionalMediaSource
struct AdditionalMediaSource: Codable {
    let liveTvID, streamKey: String
    let source: String
    let label: String
    let url: String

    enum CodingKeys: String, CodingKey {
        case liveTvID = "live_tv_id"
        case streamKey = "stream_key"
        case source, label, url
    }
}

// MARK: - TVChannelDataModel
struct ChannelDataModel: Codable {
    let liveTvID, tvName: String
    let TVDescription: String
    let slug: String
    let streamFrom: String
    let streamLabel: String
    let streamURL: String
    let thumbnailURL, posterURL: String

    enum CodingKeys: String, CodingKey {
        case liveTvID = "live_tv_id"
        case tvName = "tv_name"
        case TVDescription = "description"
        case slug
        case streamFrom = "stream_from"
        case streamLabel = "stream_label"
        case streamURL = "stream_url"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}
