//
//  MovieCategoryDataModel.swift
//  OXOO
//
//  Created by Abdul Mannan on 23/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - MovieCategoryDataModel
struct MovieCategoryDataModel: Codable {
    let genreID: String
    let name: String
    let movieDescription: String
    let slug: String
    let url: String
    let videos: [Videos]

    enum CodingKeys: String, CodingKey {
        case genreID = "genre_id"
        case name
        case movieDescription = "description"
        case slug, url, videos
    }
}


// MARK: - Videos

struct Videos: Codable {
    let videosID: String
    let title: String
    let videoDescription: String
    let slug: String
    let release: String
    let isTvseries: String
    let runtime: String
    let videoQuality: String
    let thumbnailURL: String
    let posterURL: String

    enum CodingKeys: String, CodingKey {
        case videosID = "videos_id"
        case title
        case videoDescription = "description"
        case slug, release
        case isTvseries = "is_tvseries"
        case runtime
        case videoQuality = "video_quality"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}
