// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let movieDetailDataModel = try? newJSONDecoder().decode(MovieDetailDataModel.self, from: jsonData)

import Foundation

// MARK: - MovieDetailDataModel
struct MovieDetailsDataModel: Codable {
    let videosID, title, movieDescription, slug: String
    let release, runtime: String
    let videoQuality: String
    let isTvseries, enableDownload: String
    let downloadLinks: [DownloadLink]
    let thumbnailURL, posterURL: String
    let videos: [Video]
    let genre: [Genre]
    let country: [Country]
    let director, writer, cast, castAndCrew: [Cast]
    let relatedMovie: [RelatedMovie]

    enum CodingKeys: String, CodingKey {
        case videosID = "videos_id"
        case title
        case movieDescription = "description"
        case slug, release, runtime
        case videoQuality = "video_quality"
        case isTvseries = "is_tvseries"
        case enableDownload = "enable_download"
        case downloadLinks = "download_links"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
        case videos, genre, country, director, writer, cast
        case castAndCrew = "cast_and_crew"
        case relatedMovie = "related_movie"
    }
}

// MARK: - Cast
struct Cast: Codable {
    let starID, name: String
    let url: String
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case starID = "star_id"
        case name, url
        case imageURL = "image_url"
    }
}

// MARK: - Country
//struct Country: Codable {
//    let countryID, name: String
//    let url: String
//
//    enum CodingKeys: String, CodingKey {
//        case countryID = "country_id"
//        case name, url
//    }
//}

// MARK: - DownloadLink
struct DownloadLink: Codable {
    let downloadLinkID, label, videosID, resolution: String
    let fileSize: String
    let downloadURL: String

    enum CodingKeys: String, CodingKey {
        case downloadLinkID = "download_link_id"
        case label
        case videosID = "videos_id"
        case resolution
        case fileSize = "file_size"
        case downloadURL = "download_url"
    }
}

// MARK: - Genre
//struct Genre: Codable {
//    let genreID, name: String
//    let url: String
//
//    enum CodingKeys: String, CodingKey {
//        case genreID = "genre_id"
//        case name, url
//    }
//}

// MARK: - RelatedMovie
struct RelatedMovie: Codable {
    let videosID, genre, country, title: String
    let relatedMovieDescription, slug, release, runtime: String
    let videoQuality: String
    let thumbnailURL, posterURL: String

    enum CodingKeys: String, CodingKey {
        case videosID = "videos_id"
        case genre, country, title
        case relatedMovieDescription = "description"
        case slug, release, runtime
        case videoQuality = "video_quality"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}


// MARK: - Video
struct Video: Codable {
    let videoFileID, label, streamKey, fileType: String
    let fileURL: String
    let subtitle: [String]

    enum CodingKeys: String, CodingKey {
        case videoFileID = "video_file_id"
        case label
        case streamKey = "stream_key"
        case fileType = "file_type"
        case fileURL = "file_url"
        case subtitle
    }
}
