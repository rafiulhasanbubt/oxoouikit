//
//  GenreDataModel.swift
//  OXOO
//
//  Created by Abdul Mannan on 21/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - GenreDataModelElement
struct GenreDataModel: Codable {
    let genreID: String
    let name: String
    let genreDescription: String
    let slug: String
    let url: String
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case genreID = "genre_id"
        case name
        case genreDescription = "description"
        case slug
        case url
        case imageURL = "image_url"
    }
}
