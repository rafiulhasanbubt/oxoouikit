//
//  HomeDataModel.swift
//  OXOO
//
//  Created by SpaGreen on 4/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import Foundation

struct HomeDataModel: Codable {
    let sliderType: String
    let slides: [LatestMovie]
    let genreVisible: Bool
    let genres: [Genre]
    let countryVisible: Bool
    let countries: [Country]
    let featuredTvChannel: [FeaturedTvChannel]
    let latestMovie, latestTvseries: [LatestMovie]
    let welcomeAction, comedy, horror, thriller: [MovieType]
    let action: [Action]

    enum CodingKeys: String, CodingKey {
        case sliderType = "slider_type"
        case slides
        case genreVisible = "genre_visible"
        case genres
        case countryVisible = "country_visible"
        case countries
        case featuredTvChannel = "featured_tv_channel"
        case latestMovie = "latest_movie"
        case latestTvseries = "latest_tvseries"
        case welcomeAction = "action"
        case comedy, horror, thriller
        case action = " action"
    }
}

// MARK: - Genre
struct Genre: Codable {
    let genreID: String
    let name: String
    let url: String
    let genreDescription: String
    let slug: String
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case genreID = "genre_id"
        case name
        case url
        case genreDescription = "description"
        case slug
        case imageURL = "image_url"
    }
}

// MARK: - MovieType
struct MovieType: Codable {
    let id, type, title, actionDescription: String
    let slug, url: String
    let content: [Content]

    enum CodingKeys: String, CodingKey {
        case id, type, title
        case actionDescription = "description"
        case slug, url, content
    }
}

// MARK: - Action
struct Action: Codable {
    let id, type, title, actionDescription: String
    let slug, url: String
    let content: [Content]

    enum CodingKeys: String, CodingKey {
        case id, type, title
        case actionDescription = "description"
        case slug, url, content
    }
}

// MARK: - Content
struct Content: Codable {
    let id, title, contentDescription, slug: String
    let release, runtime: String
    let videoQuality: String
    let thumbnailURL, posterURL: String
    let streamFrom, streamLabel, streamURL, isTvseries: String

    enum CodingKeys: String, CodingKey {
        case id, title
        case contentDescription = "description"
        case slug, release, runtime
        case videoQuality = "video_quality"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
        case streamFrom = "stream_from"
        case streamLabel = "stream_label"
        case streamURL = "stream_url"
        case isTvseries = "is_tvseries"
    }
}

// MARK: - Country
struct Country: Codable {
    let countryID: String?
    let name: String
    let url: String
    let countryDescription: String
    let slug: String
    let imageURL: String
    let genreID: String?

    enum CodingKeys: String, CodingKey {
        case countryID = "country_id"
        case name
        case url
        case countryDescription = "description"
        case slug
        case imageURL = "image_url"
        case genreID = "genre_id"
    }
}

// MARK: - FeaturedTvChannel
struct FeaturedTvChannel: Codable {
    let liveTvID, tvName: String
    let featuredTvChannelDescription: String
    let slug: String
    let streamFrom: String
    let streamLabel: String
    let streamURL: String
    let thumbnailURL: String
    let posterURL: String

    enum CodingKeys: String, CodingKey {
        case liveTvID = "live_tv_id"
        case tvName = "tv_name"
        case featuredTvChannelDescription = "description"
        case slug
        case streamFrom = "stream_from"
        case streamLabel = "stream_label"
        case streamURL = "stream_url"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}

// MARK: - LatestMovie
struct LatestMovie: Codable {
    let videosID, title, latestMovieDescription, slug: String
    let release: String
    let isTvseries: String?
    let runtime: String
    let videoQuality: String
    let thumbnailURL, posterURL: String

    enum CodingKeys: String, CodingKey {
        case videosID = "videos_id"
        case title
        case latestMovieDescription = "description"
        case slug, release
        case isTvseries = "is_tvseries"
        case runtime
        case videoQuality = "video_quality"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}
