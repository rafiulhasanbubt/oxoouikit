//
//  TVChannelCategoryDataModel.swift
//  OXOO
//
//  Created by Abdul Mannan on 24/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

// MARK: - AllChannelDataModel
struct TVChannelCategoryDataModel: Codable {
    let liveTvCategoryID: String
    let title: String
    let allChannelDescription: String
    let channels: [Channel]

    enum CodingKeys: String, CodingKey {
        case liveTvCategoryID = "live_tv_category_id"
        case title
        case allChannelDescription = "description"
        case channels
    }
}

// MARK: - Channel
struct Channel: Codable {
    let liveTvID, tvName: String
    let channelDescription: String
    let slug: String
    let streamFrom: String
    let streamLabel: String
    let streamURL: String
    let thumbnailURL, posterURL: String

    enum CodingKeys: String, CodingKey {
        case liveTvID = "live_tv_id"
        case tvName = "tv_name"
        case channelDescription = "description"
        case slug
        case streamFrom = "stream_from"
        case streamLabel = "stream_label"
        case streamURL = "stream_url"
        case thumbnailURL = "thumbnail_url"
        case posterURL = "poster_url"
    }
}
