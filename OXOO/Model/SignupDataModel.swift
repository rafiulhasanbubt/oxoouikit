//
//  SignupDataModel.swift
//  OXOO
//
//  Created by SpaGreen on 9/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation

//Signup Datamodel
struct SignupRequest: Codable {
    let apiSecretKey: String
    let email: String
    let password: String
    let name: String

    enum CodingKeys: String, CodingKey {
        case apiSecretKey = "api_secret_key"
        case email
        case password
        case name
    }
}

// Login datamodel
struct loginRequest: Codable {
    let email: String
    let password: String
    let requestToken: String
    
    enum CodingKeys: String, CodingKey {
        case email
        case password
        case requestToken
    }
}

// Login Response
struct LoginResponse: Codable {
    let status, userID, name, email: String
    let gender, joinDate, lastLogin: String

    enum CodingKeys: String, CodingKey {
        case status
        case userID = "user_id"
        case name, email, gender
        case joinDate = "join_date"
        case lastLogin = "last_login"
    }
}

//Request Token Response
struct RequestTokenResponse: Codable {
    let success: Bool
    let expiresAt: String
    let requestToken: String
    
    enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "api_secret_key"
    }
}

struct Auth {
    static var accountID = 0
    static var requestToken = "wv0e0d0jsl78pwgo63h7fhd9"
    static var SessionID = ""
}
