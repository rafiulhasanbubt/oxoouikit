//
//  CardView.swift
//  OXOO
//
//  Created by Abdul Mannan on 23/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CardView: UIView {
    var cornnerRadius: CGFloat = 6
    var bordarWidth:CGFloat = 0.1
    var shadowOfSetWidth: CGFloat = 2
    var shadowOfSetHeight: CGFloat = 0.0
    var shadowColour: UIColor = UIColor.clear
    var shadowOpacity: CGFloat = 0.6
    
    override func layoutSubviews() {
        layer.cornerRadius = cornnerRadius
        layer.borderWidth = bordarWidth
        layer.shadowColor = shadowColour.cgColor
        layer.shadowOffset = CGSize(width: shadowOfSetWidth, height: shadowOfSetHeight)
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornnerRadius)
        layer.shadowPath = shadowPath.cgPath
        layer.shadowOpacity = Float(shadowOpacity)
    }
}
