//
//  Insert Text Field.swift
//  OXOO
//
//  Created by SpaGreen on 1/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import Foundation
import UIKit

class InsertTextField: UITextField {
    
    private var textRectOffset : CGFloat = 20
    private var padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let placeholder = NSAttributedString(string: self.placeholder!, attributes:[NSAttributedString.Key.foregroundColor:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)])
        self.attributedPlaceholder = placeholder
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect { return bounds.inset(by: padding) }
    override func editingRect(forBounds bounds: CGRect) -> CGRect { return bounds.inset(by: padding) }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect { return bounds.inset(by: padding) }
}
