//
//  DownloadCollectionViewCell.swift
//  OXOO
//
//  Created by SpaGreen on 5/11/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class DownloadCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var downloadSourceLabel: UILabel!
    @IBOutlet weak var downloadQualityLabel: UILabel!
    @IBOutlet weak var downloadSizeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 4.0;
        self.layer.borderWidth = 0.2
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow properties
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 2
    }

}
