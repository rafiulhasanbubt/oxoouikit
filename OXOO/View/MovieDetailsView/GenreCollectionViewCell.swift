//
//  GenreCollectionViewCell.swift
//  MovieDetail
//
//  Created by SpaGreen on 19/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class GenreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var genreLabel: UILabel!
    
}
