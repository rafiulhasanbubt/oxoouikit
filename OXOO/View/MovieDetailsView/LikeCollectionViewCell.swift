//
//  LikeCollectionViewCell.swift
//  MovieDetail
//
//  Created by SpaGreen on 19/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class LikeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var likeTitleLabel: UILabel!
    @IBOutlet weak var likeReleaseLabel: UILabel!
    
    
    override func awakeFromNib() {
        //TODO: Configure the CollectionCell specific properties
        self.layer.cornerRadius = 4.0;
        self.layer.borderWidth = 0.2
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow properties
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 3
    }
}
