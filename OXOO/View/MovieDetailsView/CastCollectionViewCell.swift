//
//  CastCollectionViewCell.swift
//  MovieDetail
//
//  Created by SpaGreen on 7/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class CastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var castImageView: UIImageView!
    @IBOutlet weak var castNameLabel: UILabel!
    
    var name: String?
    var image: UIImage?
    var dataArray: [Cast]?
    var imageURL: String?
    
    func updateCastUI(castModel: Cast) {
        imageURL = castModel.url
        castNameLabel.text = castModel.name
    }
    
    override func awakeFromNib() {
        
//        castImageView.makeRounded()
        castImageView?.layer.cornerRadius = (castImageView?.frame.size.width ?? 0.0) / 2
        castImageView?.clipsToBounds = true
        castImageView?.layer.borderWidth = 1.0
        castImageView?.layer.borderColor = UIColor.white.cgColor
    }
}
