//
//  HeaderGenreCollectionViewCell.swift
//  OXOO
//
//  Created by SpaGreen on 24/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class HeaderGenreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headerGenreLabel: UILabel!

}
