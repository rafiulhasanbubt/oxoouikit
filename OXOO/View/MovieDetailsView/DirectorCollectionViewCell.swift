//
//  DirectorCollectionViewCell.swift
//  MovieDetail
//
//  Created by SpaGreen on 19/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class DirectorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var directorImageView: UIImageView!
    
    override func awakeFromNib() {
        directorImageView?.layer.cornerRadius = (directorImageView?.frame.size.width ?? 0.0) / 2
        directorImageView?.clipsToBounds = true
        directorImageView?.layer.borderWidth = 1.0
        directorImageView?.layer.borderColor = UIColor.white.cgColor
    }
}
