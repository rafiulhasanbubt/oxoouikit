//
//  TVSeriesCastViewCell.swift
//  OXOO
//
//  Created by SpaGreen on 30/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class TVSeriesCastViewCell: UICollectionViewCell {
    
    @IBOutlet weak var castLabel: UILabel!
    @IBOutlet weak var castImageView: UIImageView!
    
    override func awakeFromNib() {
        castImageView?.layer.cornerRadius = (castImageView?.frame.size.width ?? 0.0) / 2
        castImageView?.clipsToBounds = true
        castImageView?.layer.borderWidth = 1.0
        castImageView?.layer.borderColor = UIColor.white.cgColor
    }
}
