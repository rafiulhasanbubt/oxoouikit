//
//  EpisodeViewCell.swift
//  OXOO
//
//  Created by SpaGreen on 31/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class EpisodeViewCell: UICollectionViewCell {
    
    @IBOutlet weak var episodeImageView: UIImageView!
    @IBOutlet weak var episodeTitleLabel: UILabel!
    
    override func awakeFromNib() {
        //TODO: Configure the CollectionCell specific properties
        self.layer.cornerRadius = 4.0;
        self.layer.borderWidth = 0.2
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow properties
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 3
    }
}
