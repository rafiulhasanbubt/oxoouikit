//
//  TVSeriesHeaderGenreViewCell.swift
//  OXOO
//
//  Created by SpaGreen on 30/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class TVSeriesHeaderGenreViewCell: UICollectionViewCell {
    
    @IBOutlet weak var genreLabel: UILabel!
    
}
