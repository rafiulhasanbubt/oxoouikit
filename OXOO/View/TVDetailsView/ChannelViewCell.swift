//
//  ChannelViewCell.swift
//  OXOO
//
//  Created by SpaGreen on 12/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class ChannelViewCell: UICollectionViewCell {
    @IBOutlet weak var channelImageView: UIImageView!
    @IBOutlet weak var channelNameLabel: UILabel!
    
    var channelName: String?
    var channelImage: UIImage?
    
    func updateUIView(){
        channelNameLabel.text = channelName
        channelImageView.image = channelImage
    }
}
