//
//  TVDetailView.swift
//  OXOO
//
//  Created by SpaGreen on 11/12/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class TVDetailView: UIView {

    @IBOutlet weak var tvImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sloganLabel: UILabel!

    @IBAction func shareButtonPressed(_ sender: UIButton) {
        
    }
}
