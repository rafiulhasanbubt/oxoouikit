//
//  LatestMovieTableViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 20/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

protocol LatestCellDelegate: class {
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: LatestMovieTableViewCell, videoID: String)
}

class LatestMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var latestMovieCollectionView: UICollectionView!
    
    class var latestMovieCell : LatestMovieTableViewCell {
        let cell = Bundle.main.loadNibNamed("LatestMovieTableViewCell", owner: self, options: nil)?.last
        return cell as! LatestMovieTableViewCell
    }
    
    var latestMovieArray = [LatestMovie]()
    var latestDelegate: LatestCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        latestMovieCollectionView.dataSource = self
        latestMovieCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumInteritemSpacing = 8.0
        self.latestMovieCollectionView.collectionViewLayout = flowLayout
        
        let movieNib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        latestMovieCollectionView.register(movieNib, forCellWithReuseIdentifier: "movieCell")

        getlatestMovieData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func moreButtonTapped(_ sender: UIButton) {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let detailController = storyBoard.instantiateViewController(withIdentifier:"MoviesViewController") as! MoviesViewController
//        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func getlatestMovieData() {
        let homeurl = Webservice.homeContent
        getDataWithHeaderArrayCodable(url: homeurl) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieModel in result.latestMovie {
                    self.latestMovieArray.append(movieModel)
                }
                
                DispatchQueue.main.async {
                    self.latestMovieCollectionView.reloadData()
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

extension LatestMovieTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return latestMovieArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        let moviedata = latestMovieArray[indexPath.row]
        cell.configureMovieCell(movieData: moviedata)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoId = latestMovieArray[indexPath.row].videosID
        let cell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        self.latestDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, videoID: videoId)
    }
}
