//
//  ActionTableViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 20/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

protocol ActionCellDelegate: class {
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: ActionTableViewCell, videoID: String)
}

class ActionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    class var categoryCell : ActionTableViewCell {
        let cell = Bundle.main.loadNibNamed("ActionTableViewCell", owner: self, options: nil)?.last
        return cell as! ActionTableViewCell
    }
    
    var videoArray = [MovieType]()
    var contentArray = [Content]()
    var actionDelegate: ActionCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.categoryCollectionView.dataSource = self
        self.categoryCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumInteritemSpacing = 8.0
        self.categoryCollectionView.collectionViewLayout = flowLayout
        
        let contentNib = UINib(nibName: "ContentCollectionViewCell", bundle: nil)
        categoryCollectionView.register(contentNib, forCellWithReuseIdentifier: "contentCell")
        
        getCateData()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func moreButtonTapped(_ sender: UIButton) {
//        let getType = videoArray[indexPath.row].id
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let detailController = storyBoard.instantiateViewController(withIdentifier:"CategoryViewController") as! GenreCategoryViewController
//        detailController.tvID = getType
//        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func getCateData() {
        
        let baseURL = Webservice.homeContent
        getDataWithHeaderCodable(url: baseURL) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieTypeModel in result.welcomeAction {
                    self.videoArray.append(movieTypeModel)
                    for videoModel in movieTypeModel.content {
                        self.contentArray.append(videoModel)
                    }
                }
                
                DispatchQueue.main.async {
                    self.categoryCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}


extension ActionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCell", for: indexPath) as! ContentCollectionViewCell
        
        let contentData = contentArray[indexPath.row]
        cell.configureContentCell(contentData: contentData)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoId = contentArray[indexPath.row].id
        let cell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        self.actionDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, videoID: videoId)
    }
}
