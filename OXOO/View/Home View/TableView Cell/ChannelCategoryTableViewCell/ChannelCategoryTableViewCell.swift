//
//  ChannelCategoryTableViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 26/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

protocol CustomTVCellDelegate: class {
    func collectionView(collectioncell: ChannelCollectionViewCell?, didTappedInTableview TableCell: ChannelCategoryTableViewCell, detailID: String)
}

class ChannelCategoryTableViewCell: UITableViewCell {
    
    var channelCell: ChannelCategoryTableViewCell {
        let cell = Bundle.main.loadNibNamed("ChannelCategoryTableViewCell", owner: self, options: nil)?.last
        return cell as! ChannelCategoryTableViewCell
    }
    
    @IBOutlet weak var channelCategoryLabel: UILabel!
    @IBOutlet weak var channelCategoryCollectionView: UICollectionView!
    
    var channelArray: [Channel]?
    var cellDelegate: CustomTVCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        channelCategoryCollectionView.dataSource = self
        channelCategoryCollectionView.delegate = self
        // Initialization code
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 140, height: 110)
        flowLayout.minimumInteritemSpacing = 8.0
        self.channelCategoryCollectionView.collectionViewLayout = flowLayout
        
        let channelNib = UINib(nibName: "ChannelCollectionViewCell", bundle: nil)
        channelCategoryCollectionView.register(channelNib, forCellWithReuseIdentifier: "channelCell")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)        
        // Configure the view for the selected state
    }
}


extension ChannelCategoryTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return channelArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "channelCell", for: indexPath) as! ChannelCollectionViewCell
        
        cell.channelNameLAbel.text = channelArray?[indexPath.row].tvName
        cell.channelImageView.image = UIImage(named: "dummy TV")
        
        guard let ImageURL = URL(string: channelArray?[indexPath.row].posterURL ?? "") else { return cell }
        let data = try? Data(contentsOf: ImageURL)
        if let imageData = data {
            let image = UIImage(data: imageData)
            cell.channelImageView.image = image
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tvID = channelArray![indexPath.row].liveTvID
        let cell = collectionView.cellForItem(at: indexPath) as? ChannelCollectionViewCell
        self.cellDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, detailID: tvID)
    }
}

