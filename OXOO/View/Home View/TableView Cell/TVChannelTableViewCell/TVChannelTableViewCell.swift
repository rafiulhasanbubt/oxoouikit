//
//  TVChannelTableViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 20/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

protocol TVCellDelegate: class {
    func collectionView(collectioncell: ChannelCollectionViewCell?, didTappedInTableview TableCell: TVChannelTableViewCell, detailID: String)
}

class TVChannelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tvChannelCollectionView: UICollectionView!
    
    class var tvChannelCell : TVChannelTableViewCell {
        let cell = Bundle.main.loadNibNamed("TVChannelTableViewCell", owner: self, options: nil)?.last
        return cell as! TVChannelTableViewCell
    }
    
    var tvChannelArray = [FeaturedTvChannel]()
    var tvDelegate: TVCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tvChannelCollectionView.dataSource = self
        tvChannelCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 140, height: 110)
        flowLayout.minimumInteritemSpacing = 8.0
        self.tvChannelCollectionView.collectionViewLayout = flowLayout
        
        let channelNib = UINib(nibName: "ChannelCollectionViewCell", bundle: nil)
        tvChannelCollectionView.register(channelNib, forCellWithReuseIdentifier: "channelCell")
        
        getTVChannelData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func moreButtonTapped(_ sender: UIButton) {
        
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let detailController = storyBoard.instantiateViewController(withIdentifier:"LiveTVViewController") as! LiveTVViewController
//        self.navigationController.pushViewController(detailController, animated: true)
    }
    
    func getTVChannelData() {
        let baseURL = Webservice.homeContent
        
        getDataWithHeaderCodable(url: baseURL) { (response) in
            let data = response
            
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieModel in result.featuredTvChannel {
                    self.tvChannelArray.append(movieModel)
                }
                
                DispatchQueue.main.async {
                    self.tvChannelCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

extension TVChannelTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tvChannelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "channelCell", for: indexPath) as! ChannelCollectionViewCell
        let channels = tvChannelArray[indexPath.row]
        cell.configureTVCell(channel: channels)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tvID = tvChannelArray[indexPath.row].liveTvID
        let cell = collectionView.cellForItem(at: indexPath) as? ChannelCollectionViewCell
        self.tvDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, detailID: tvID)
    }
}
