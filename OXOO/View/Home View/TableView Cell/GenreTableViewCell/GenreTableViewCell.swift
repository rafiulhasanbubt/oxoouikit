//
//  GenericTableViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 20/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//1071260344048

import UIKit

protocol GenreCellDelegate {
    func collectionView(collectionCell: CategoryCollectionViewCell?, didTappedInTableview TableCell: GenreTableViewCell, genreID: String)
}

class GenreTableViewCell: UITableViewCell {

    @IBOutlet weak var genreCollectionView: UICollectionView!
    
    class var genericCell : GenreTableViewCell {
        let cell = Bundle.main.loadNibNamed("GenreTableViewCell", owner: self, options: nil)?.last
        return cell as! GenreTableViewCell
    }
    
    var genreArray = [Genre]()
    var genreDelegate: GenreCellDelegate? //define delegate    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        genreCollectionView.dataSource = self
        genreCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 100, height: 80)
        flowLayout.minimumInteritemSpacing = 8.0
        self.genreCollectionView.collectionViewLayout = flowLayout
        
        let genreNib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        genreCollectionView.register(genreNib, forCellWithReuseIdentifier: "categoryCell")
        
        getGenreData()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func getGenreData() {
        let baseURL = Webservice.homeContent
        getDataWithHeaderCodable(url: baseURL) { (response) in
            let data = response
            
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieModel in result.genres {
                    self.genreArray.append(movieModel)
                }
                
                DispatchQueue.main.async {
                    self.genreCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

extension GenreTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genreArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell

        let genrecell = genreArray[indexPath.row]
        cell.configureGenreCell(genreCell: genrecell)
        cell.gradientColor()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let genreID = genreArray[indexPath.row].genreID
        let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell
        self.genreDelegate?.collectionView(collectionCell: cell, didTappedInTableview: self, genreID: genreID)
    }

}
