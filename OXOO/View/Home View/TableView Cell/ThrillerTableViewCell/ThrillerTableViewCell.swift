//
//  ThrillerTableViewCell.swift
//  OxooHomePage
//
//  Created by SpaGreen on 8/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import UIKit

protocol ThrillerCellDelegate: class {
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: ThrillerTableViewCell, videoID: String)
}

class ThrillerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thrillerCollectionView: UICollectionView!
    
    var thrillerTypeArray = [MovieType]()
    var contentItemArray = [Content]()
    var thrillerDelegate: ThrillerCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.thrillerCollectionView.dataSource = self
        self.thrillerCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumInteritemSpacing = 8.0
        self.thrillerCollectionView.collectionViewLayout = flowLayout
        
        let contentNib = UINib(nibName: "ContentCollectionViewCell", bundle: nil)
        thrillerCollectionView.register(contentNib, forCellWithReuseIdentifier: "contentCell")
        
        getThrillerData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func getThrillerData() {
        let baseURL = Webservice.homeContent
        
        getDataWithHeaderCodable(url: baseURL) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieTypeModel in result.horror {
                    self.thrillerTypeArray.append(movieTypeModel)
                    for videoModel in movieTypeModel.content {
                        self.contentItemArray.append(videoModel)
                    }
                }
                
                DispatchQueue.main.async {
                    self.thrillerCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

extension ThrillerTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentItemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCell", for: indexPath) as! ContentCollectionViewCell
        
        let contentData = contentItemArray[indexPath.row]
        cell.configureContentCell(contentData: contentData)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoId = contentItemArray[indexPath.row].id
        let cell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        self.thrillerDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, videoID: videoId)
    }
}
