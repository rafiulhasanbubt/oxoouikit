//
//  ComedyTableViewCell.swift
//  OxooHomePage
//
//  Created by SpaGreen on 7/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import UIKit

protocol ComedyCellDelegate: class {
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: ComedyTableViewCell, videoID: String)
}

class ComedyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var comedyCollectionView: UICollectionView!
    
    var movieTypeArray = [MovieType]()
    var contentItemArray = [Content]()
    var codemyDelegate: ComedyCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.comedyCollectionView.dataSource = self
        self.comedyCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumInteritemSpacing = 8.0
        self.comedyCollectionView.collectionViewLayout = flowLayout
        
        let contentNib = UINib(nibName: "ContentCollectionViewCell", bundle: nil)
        comedyCollectionView.register(contentNib, forCellWithReuseIdentifier: "contentCell")
        
        getComedyData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func getComedyData() {
        let baseURL = Webservice.homeContent
        
        getDataWithHeaderCodable(url: baseURL) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieTypeModel in result.comedy {
                    self.movieTypeArray.append(movieTypeModel)
                    for videoModel in movieTypeModel.content {
                        self.contentItemArray.append(videoModel)
                    }
                }
                
                DispatchQueue.main.async {
                    self.comedyCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

extension ComedyTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentItemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCell", for: indexPath) as! ContentCollectionViewCell
        
        let contentData = contentItemArray[indexPath.row]
        cell.configureContentCell(contentData: contentData)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoId = contentItemArray[indexPath.row].id
        let cell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        self.codemyDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, videoID: videoId)
    }
    
}
