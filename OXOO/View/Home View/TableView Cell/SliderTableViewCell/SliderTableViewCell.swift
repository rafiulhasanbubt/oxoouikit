//
//  SliderTableViewCell.swift
//  OxooHomePage
//
//  Created by SpaGreen on 9/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import UIKit

protocol SliderCellDelegate: class {
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: SliderTableViewCell, sliderID: String)
}

class SliderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var pageView: UIPageControl!
    
    var sliderArray = [LatestMovie]()
    var sliderDelegate: SliderCellDelegate?
    var timer = Timer()
    var counter = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.sliderCollectionView.dataSource = self
        self.sliderCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumInteritemSpacing = 8.0
        self.sliderCollectionView.collectionViewLayout = flowLayout
        
        let movieNib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        sliderCollectionView.register(movieNib, forCellWithReuseIdentifier: "movieCell")
        
        // paging part start
        pageView.numberOfPages = sliderArray.count
        pageView.currentPage = 0
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
        
        getSliderData()
    }
    
    //paging part
    @objc func changeImage() {
        if counter < sliderArray.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageView.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageView.currentPage = counter
            counter = 1
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func getSliderData() {
        let baseURL = Webservice.homeContent
        getDataWithHeaderCodable(url: baseURL) { (response) in
            
            let data = response
            
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieTypeModel in result.slides {
                    self.sliderArray.append(movieTypeModel)
                }
                
                DispatchQueue.main.async {
                    self.sliderCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}


extension SliderTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        let moviedata = sliderArray[indexPath.row]
        cell.configureMovieCell(movieData: moviedata)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoId = sliderArray[indexPath.row].videosID
        let cell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        self.sliderDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, sliderID: videoId)        
    }
}
