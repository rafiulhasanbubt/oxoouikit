//
//  CountryTableViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 20/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

protocol CountryCellDelegate: class {
    func collectionView(collectioncell: ChannelCollectionViewCell?, didTappedInTableview TableCell: CountryTableViewCell, countryID: String)
}

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var countryCollectionView: UICollectionView!
    
    class var countryCell : CountryTableViewCell {
        let cell = Bundle.main.loadNibNamed("CountryTableViewCell", owner: self, options: nil)?.last
        return cell as! CountryTableViewCell
    }
    
    var countryArray = [Country]()
    var countryDelegate: CountryCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        countryCollectionView.dataSource = self
        countryCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 100, height: 80)
        flowLayout.minimumInteritemSpacing = 8.0
        self.countryCollectionView.collectionViewLayout = flowLayout
        
        let countryNib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        countryCollectionView.register(countryNib, forCellWithReuseIdentifier: "categoryCell")

        getCountryData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func getCountryData() {
        let baseURL = Webservice.homeContent
        
        getDataWithHeaderCodable(url: baseURL) { (response) in
            
            let data = response
            
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieModel in result.countries {
                    self.countryArray.append(movieModel)
                }
                
                DispatchQueue.main.async {
                    self.countryCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }

}

extension CountryTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return countryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
        
        let countrytype = countryArray[indexPath.row]
        cell.configureCountryCell(countryCell: countrytype)
        cell.gradientColor()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let countryId = countryArray[indexPath.row].countryID!
        let cell = collectionView.cellForItem(at: indexPath) as? ChannelCollectionViewCell
        self.countryDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, countryID: countryId)
    }
    
}
