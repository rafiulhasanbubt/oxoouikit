//
//  HorrorTableViewCell.swift
//  OxooHomePage
//
//  Created by SpaGreen on 8/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import UIKit

protocol HorrorCellDelegate: class {
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: HorrorTableViewCell, videoID: String)
}

class HorrorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var horrorCollectionView: UICollectionView!
    
    var horrorTypeArray = [MovieType]()
    var contentItemArray = [Content]()
    var horrorDelegate: HorrorCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.horrorCollectionView.dataSource = self
        self.horrorCollectionView.delegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumInteritemSpacing = 8.0
        self.horrorCollectionView.collectionViewLayout = flowLayout
        
        let contentNib = UINib(nibName: "ContentCollectionViewCell", bundle: nil)
        horrorCollectionView.register(contentNib, forCellWithReuseIdentifier: "contentCell")
        
        getHorrorData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func getHorrorData() {
        let baseURL = Webservice.homeContent
        
        getDataWithHeaderCodable(url: baseURL) { (response) in
            let data = response
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieTypeModel in result.horror {
                    self.horrorTypeArray.append(movieTypeModel)
                    for videoModel in movieTypeModel.content {
                        self.contentItemArray.append(videoModel)
                    }
                }
                
                DispatchQueue.main.async {
                    self.horrorCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

extension HorrorTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentItemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCell", for: indexPath) as! ContentCollectionViewCell
        
        let contentData = contentItemArray[indexPath.row]
        cell.configureContentCell(contentData: contentData)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let videoId = contentItemArray[indexPath.row].id
        let cell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        self.horrorDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, videoID: videoId)
    }
}

