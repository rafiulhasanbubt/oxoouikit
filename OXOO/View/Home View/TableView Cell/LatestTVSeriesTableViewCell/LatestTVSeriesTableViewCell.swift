//
//  LatestTVSeriesTableViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 20/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

protocol LatestTVCellDelegate {
    func collectionView(collectioncell: MovieCollectionViewCell?, didTappedInTableview TableCell: LatestTVSeriesTableViewCell, videoID: String)
}

class LatestTVSeriesTableViewCell: UITableViewCell {

    @IBOutlet weak var tvSeriesCollectionView: UICollectionView!
    
    class var latestTVSeriesCell : LatestTVSeriesTableViewCell {
        let cell = Bundle.main.loadNibNamed("LatestTVSeriesTableViewCell", owner: self, options: nil)?.last
        return cell as! LatestTVSeriesTableViewCell
    }
    
    var latestTVSeriesArray = [LatestMovie]()
    var latestTVDelegate: LatestTVCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        tvSeriesCollectionView.delegate = self
        tvSeriesCollectionView.dataSource = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 130, height: 250)
        flowLayout.minimumInteritemSpacing = 8.0
        self.tvSeriesCollectionView.collectionViewLayout = flowLayout
        
        let movieNib = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        tvSeriesCollectionView.register(movieNib, forCellWithReuseIdentifier: "movieCell")

        getLatestTVSeriesData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func moreButtonTapped(_ sender: UIButton) {
        
    }
    
    func getLatestTVSeriesData() {
        let Username = "admin"
        let password = "1234"
        let loginData = String(format: "%@:%@", Username, password).data(using: String.Encoding.utf8)!
        let base64LoginData = loginData.base64EncodedString()
        
        // create the request
        let url = URL(string: "http://demo.redtvlive.com/v320/api/v100/home_content")!
        var request = URLRequest(url: url)
        request.addValue("this-is-my-key", forHTTPHeaderField: "API-KEY")
        request.httpMethod = "GET"
        request.setValue("Basic \(base64LoginData)", forHTTPHeaderField: "Authorization")
        
        //making the request
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                debugPrint(error.debugDescription)
                return
            }
            
            guard let data = data else { return }
            
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(HomeDataModel.self, from: data)
                
                for movieModel in result.latestTvseries {
                    self.latestTVSeriesArray.append(movieModel)
                }
                
                DispatchQueue.main.async {
                    self.tvSeriesCollectionView.reloadData()
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }

    
}


extension LatestTVSeriesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return latestTVSeriesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        let moviedata = latestTVSeriesArray[indexPath.row]
        cell.configureMovieCell(movieData: moviedata)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let TVSeriesId = latestTVSeriesArray[indexPath.row].videosID
        let cell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        self.latestTVDelegate?.collectionView(collectioncell: cell, didTappedInTableview: self, videoID: TVSeriesId)
    }
    
}
