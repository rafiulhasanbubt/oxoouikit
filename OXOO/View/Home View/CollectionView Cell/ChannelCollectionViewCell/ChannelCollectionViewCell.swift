//
//  ChannelCollectionViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 16/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class ChannelCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var channelView: UIView!
    @IBOutlet weak var channelImageView: UIImageView!
    @IBOutlet weak var channelNameLAbel: UILabel!
    
    class var channelCell : ChannelCollectionViewCell {
        let cell = Bundle.main.loadNibNamed("ChannelCollectionViewCell", owner: self, options: nil)?.last
        return cell as! ChannelCollectionViewCell
    }
    
    func configureTVCell(channel: FeaturedTvChannel) {
        channelNameLAbel.text = channel.tvName
        guard let posterURL = URL(string: channel.posterURL) else { return }
        let data = try? Data(contentsOf: posterURL)
        if let imageData = data {
            let image = UIImage(data: imageData)
            channelImageView.image = image
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //TODO: Configure the cell properties
        self.backgroundColor = .white
        
        //TODO: Configure the CollectionCell specific properties
        self.layer.cornerRadius = 6.0;
        self.layer.borderWidth = 0.2
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow properties
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 2
        
    }
    
}
