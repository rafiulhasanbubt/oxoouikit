//
//  SliderCollectionViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 16/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class SliderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var sliderImageView: UIImageView!
    
    class var sliderCell : SliderCollectionViewCell {
        let cell = Bundle.main.loadNibNamed("SliderCollectionViewCell", owner: self, options: nil)?.last
        return cell as! SliderCollectionViewCell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        sliderImageView.layer.cornerRadius = 10.0;
        sliderImageView.layer.borderWidth = 1.0
        sliderImageView.layer.borderColor = UIColor.clear.cgColor
    }

}
