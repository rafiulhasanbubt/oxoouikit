//
//  MovieCollectionViewCell.swift
//  OXOO
//
//  Created by Abdul Mannan on 16/10/19.
//  Copyright © 2019 Abdul Mannan. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieCardView: UIView!
    @IBOutlet weak var movieView: UIView!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!
    
    class var movieCell : MovieCollectionViewCell {
        let cell = Bundle.main.loadNibNamed("MovieCollectionViewCell", owner: self, options: nil)?.last
        return cell as! MovieCollectionViewCell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //TODO: Configure the cell properties
        self.backgroundColor = .white
        
        //TODO: Configure the CollectionCell specific properties
        self.layer.cornerRadius = 6.0;
        self.layer.borderWidth = 0.2
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow properties
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 2
    }
    
    func configureMovieCell(movieData: LatestMovie) {
        movieTitleLabel.text = movieData.title
        categoryLabel.text = movieData.videoQuality
        releaseLabel.text = movieData.release
        
        guard let ImageURL = URL(string: movieData.thumbnailURL) else { return }
        let data = try? Data(contentsOf: ImageURL)
        if let imageData = data {
            let image = UIImage(data: imageData)
            movieImageView.image = image
        }
    }
    
    func updateMovieCell(movieData: MovieDataModel) {
        movieTitleLabel.text = movieData.title
        categoryLabel.text = movieData.videoQuality
        releaseLabel.text = movieData.release
        
        guard let ImageURL = URL(string: movieData.posterURL) else { return }
        let data = try? Data(contentsOf: ImageURL)
        if let imageData = data {
            let image = UIImage(data: imageData)
            movieImageView.image = image
        }
    }
}
