//
//  ContentCollectionViewCell.swift
//  OxooHomePage
//
//  Created by SpaGreen on 7/1/20.
//  Copyright © 2020 Abdul Mannan. All rights reserved.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var contentTitleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        //TODO: Configure the cell properties
        self.backgroundColor = .white
        
        //TODO: Configure the CollectionCell specific properties
        self.layer.cornerRadius = 6.0;
        self.layer.borderWidth = 0.2
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow properties
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 2
    }

    func configureContentCell(contentData: Content) {
        contentTitleLabel.text = contentData.title
        categoryLabel.text = contentData.videoQuality
        releaseLabel.text = contentData.release
        
        guard let ImageURL = URL(string: contentData.thumbnailURL) else { return }
        let data = try? Data(contentsOf: ImageURL)
        if let imageData = data {
            let image = UIImage(data: imageData)
            contentImageView.image = image
        }
    }
}
